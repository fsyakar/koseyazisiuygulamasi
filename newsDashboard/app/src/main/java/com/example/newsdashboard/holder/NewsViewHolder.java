package com.example.newsdashboard.holder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.newsdashboard.R;

public class NewsViewHolder extends RecyclerView.ViewHolder {
    public ImageView image;
    public TextView title;
    public TextView text;
    public TextView time;

    public NewsViewHolder(@NonNull View itemView){
        super(itemView);
        image = itemView.findViewById(R.id.newsImage);
        title = itemView.findViewById(R.id.newsTitle);
        text = itemView.findViewById(R.id.newsText);
        time = itemView.findViewById(R.id.newsTime);
    }
}