package com.example.newsdashboard.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.newsdashboard.model.AuthorModel;
import com.example.newsdashboard.model.FilterDayModel;
import com.example.newsdashboard.model.NewsModel;
import com.example.newsdashboard.model.SettingModel;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME ="general.db"; // Generic
    private static final String TABLE_NAME ="settings_table";// For settings
    private static final String TABLE_NAME2 ="author_table"; // For authors
    private static final String TABLE_NAME3 ="news_table"; // For news
    private static final String TABLE_NAME4 ="news_table_filter"; // For news
    private static final String TABLE_NAME5 ="filter_day_table"; // For news

    private static final String ID ="ID"; // Generic
    private static final String UPDATE_INTERVAL ="updateInterval"; // For settings
    private static final String AUTHOR_NAME ="authorName"; // For authors
    private static final String FILTER_DAY ="filterDay"; // For filter day
    private static final String FILTER_LIMIT ="filterLimit"; // For filter day

    private static final String NEWS_ID="newsId";
    private static final String AUTHOR="author"; // For
    private static final String TITLE="title";
    private static final String DESCRIPTION="description";
    private static final String URL="url";
    private static final String URL_TO_IMAGE="urlToImage";
    private static final String PUBLISHED_AT="publishedAt";
    private static final String CONTENT="content";


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
        //for test to call onCreate
        //SQLiteDatabase db=this.getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE "+TABLE_NAME+" ("+ID+" INTEGER PRIMARY KEY AUTOINCREMENT, " + UPDATE_INTERVAL + " TEXT)");
        db.execSQL("CREATE TABLE "+TABLE_NAME2+" ("+ID+" INTEGER PRIMARY KEY AUTOINCREMENT, " + AUTHOR_NAME + " TEXT)");
        db.execSQL("CREATE TABLE "+TABLE_NAME3+" ("+ID+" INTEGER PRIMARY KEY AUTOINCREMENT, " + NEWS_ID + " INTEGER, " + AUTHOR + " TEXT, " + TITLE +" TEXT, " + DESCRIPTION +" TEXT, " + URL +" TEXT, " + URL_TO_IMAGE +" TEXT, "+PUBLISHED_AT+" TEXT, "+CONTENT+" TEXT)");
        db.execSQL("CREATE TABLE "+TABLE_NAME4+" ("+ID+" INTEGER PRIMARY KEY AUTOINCREMENT, " + NEWS_ID + " INTEGER, " + AUTHOR + " TEXT, " + TITLE +" TEXT, " + DESCRIPTION +" TEXT, " + URL +" TEXT, " + URL_TO_IMAGE +" TEXT, "+PUBLISHED_AT+" TEXT, "+CONTENT+" TEXT)");
        db.execSQL("CREATE TABLE "+TABLE_NAME5+" ("+ID+" INTEGER PRIMARY KEY AUTOINCREMENT, " + FILTER_DAY + " INTEGER, "+ FILTER_LIMIT + " INTEGER)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_NAME2);
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_NAME3);
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_NAME4);
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_NAME5);
        onCreate(db);
    }

    public boolean insertSetting(String updateInterval){
        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues contentValues=new ContentValues();

        contentValues.put(UPDATE_INTERVAL,updateInterval);

        long result= db.insert(TABLE_NAME,null,contentValues); // -1-> failed else returns ID
        return result != -1;

    }

    public List<SettingModel> getAllSettings(){
        SQLiteDatabase db=this.getWritableDatabase();
        List<SettingModel> list=new ArrayList<>();
        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_NAME,null);

        while(cursor.moveToNext()){
            list.add(new SettingModel(cursor.getString(1)));
        }
        return list;
    }

    public SettingModel getSettingModel(String updateInterval){
        SQLiteDatabase db=this.getWritableDatabase();
        SettingModel item = null;

        Cursor cursor = db.rawQuery("SELECT * FROM "+TABLE_NAME + " WHERE " + UPDATE_INTERVAL + " = ?",new String[]{ String.valueOf(updateInterval) });

        if( cursor.moveToFirst()){
            item= new SettingModel(cursor.getString(1));
        }
        return item;
    }

    public boolean updateSettings(int id,String updateInterval){
        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues contentValues=new ContentValues();

        contentValues.put(UPDATE_INTERVAL,updateInterval);

        db.update(TABLE_NAME, contentValues,ID + " = ?", new String[]{ String.valueOf(id) });
        return true;
    }

    public int deleteSetting(String updateInterval){
        SQLiteDatabase db =this.getWritableDatabase();

        return db.delete(TABLE_NAME,UPDATE_INTERVAL+" = ?" , new String[]{ String.valueOf(updateInterval)});
    }

    public void deleteAllSettings(){
        SQLiteDatabase db =this.getWritableDatabase();
        db.execSQL("delete from "+ TABLE_NAME);
    }

    public boolean insertAuthor(String authorName){
        if(this.getAuthorModel(authorName)!=null){
            return false;
        }

        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues contentValues=new ContentValues();

        contentValues.put(AUTHOR_NAME,authorName);

        long result= db.insert(TABLE_NAME2,null,contentValues); // -1-> falied else returns ID
        return result != -1;

    }

    public List<AuthorModel> getAllAuthors(){
        SQLiteDatabase db=this.getWritableDatabase();
        List<AuthorModel> list=new ArrayList<>();
        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_NAME2,null);

        while(cursor.moveToNext()){
            list.add(new AuthorModel(cursor.getString(1)));
        }
        return list;
    }

    public AuthorModel getAuthorModel(String name){
        SQLiteDatabase db=this.getWritableDatabase();
        AuthorModel item = null;

        Cursor cursor = db.rawQuery("SELECT * FROM "+TABLE_NAME2 + " WHERE " + AUTHOR_NAME + " = ?",new String[]{ String.valueOf(name) });

        if( cursor.moveToFirst()){
            item= new AuthorModel(cursor.getString(1));
        }
        return item;
    }

    public boolean updateAuthors(int id,String name){
        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues contentValues=new ContentValues();

        contentValues.put(AUTHOR_NAME,name);

        db.update(TABLE_NAME2, contentValues,ID + " = ?", new String[]{ String.valueOf(id) });
        return true;
    }

    public int deleteAuthor(String name){
        SQLiteDatabase db =this.getWritableDatabase();

        return db.delete(TABLE_NAME2,AUTHOR_NAME+" = ?" , new String[]{ String.valueOf(name)});
    }

    public void deleteAllAuthors(){
        SQLiteDatabase db =this.getWritableDatabase();
        db.execSQL("delete from "+ TABLE_NAME2);
    }

    public boolean insertNews(int newsId,String author, String title, String description, String url, String urlToImage, String publishedAt, String content){
        if(this.getNewsModel(newsId)!=null){
            return false;
        }

        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues contentValues=new ContentValues();

        contentValues.put(NEWS_ID,newsId);
        contentValues.put(AUTHOR,author);
        contentValues.put(TITLE,title);
        contentValues.put(DESCRIPTION,description);
        contentValues.put(URL,url);
        contentValues.put(URL_TO_IMAGE,urlToImage);
        contentValues.put(PUBLISHED_AT,publishedAt);
        contentValues.put(CONTENT,content);

        long result= db.insert(TABLE_NAME3,null,contentValues); // -1-> failed else returns ID
        return result != -1;

    }

    public List<NewsModel> getAllNews(){
        SQLiteDatabase db=this.getWritableDatabase();
        List<NewsModel> list=new ArrayList<>();
        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_NAME3,null);

        while(cursor.moveToNext()){
            list.add(new NewsModel(cursor.getInt(1),cursor.getString(2),cursor.getString(3),cursor.getString(4),cursor.getString(5),cursor.getString(6),cursor.getString(7),cursor.getString(8)));
        }
        return list;
    }

    public NewsModel getNewsModel(int newsId){
        SQLiteDatabase db=this.getWritableDatabase();
        NewsModel item = null;

        Cursor cursor = db.rawQuery("SELECT * FROM "+TABLE_NAME3 + " WHERE " + NEWS_ID + " = ?",new String[]{ String.valueOf(newsId) });

        if( cursor.moveToFirst()){
            item= new NewsModel(cursor.getInt(1),cursor.getString(2),cursor.getString(3),cursor.getString(4),cursor.getString(5),cursor.getString(6),cursor.getString(7),cursor.getString(8));
        }
        return item;
    }

    public boolean updateNews(int newsId,String author, String title, String description, String url, String urlToImage, String publishedAt, String content){
        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues contentValues=new ContentValues();

        contentValues.put(NEWS_ID,newsId);
        contentValues.put(AUTHOR,author);
        contentValues.put(TITLE,title);
        contentValues.put(DESCRIPTION,description);
        contentValues.put(URL,url);
        contentValues.put(URL_TO_IMAGE,urlToImage);
        contentValues.put(PUBLISHED_AT,publishedAt);
        contentValues.put(CONTENT,content);

        db.update(TABLE_NAME3, contentValues,NEWS_ID + " = ?", new String[]{ String.valueOf(newsId) });
        return true;
    }

    public int deleteNews(int newsId){
        SQLiteDatabase db =this.getWritableDatabase();

        int result=db.delete(TABLE_NAME3,NEWS_ID+" = ?" , new String[]{ String.valueOf(newsId)});
        return result;
    }

    public void deleteAllNews(){
        SQLiteDatabase db =this.getWritableDatabase();
        db.execSQL("delete from "+ TABLE_NAME3);
    }

    public boolean insertNewsFilter(int newsId,String author, String title, String description,
                                    String url, String urlToImage, String publishedAt, String content){
        if(this.getNewsModel(newsId)!=null){
            return false;
        }

        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues contentValues=new ContentValues();

        contentValues.put(NEWS_ID,newsId);
        contentValues.put(AUTHOR,author);
        contentValues.put(TITLE,title);
        contentValues.put(DESCRIPTION,description);
        contentValues.put(URL,url);
        contentValues.put(URL_TO_IMAGE,urlToImage);
        contentValues.put(PUBLISHED_AT,publishedAt);
        contentValues.put(CONTENT,content);

        long result= db.insert(TABLE_NAME4,null,contentValues); // -1-> failed else returns ID
        return result != -1;

    }

    public List<NewsModel> getAllNewsFilter(){
        SQLiteDatabase db=this.getWritableDatabase();
        List<NewsModel> list=new ArrayList<>();
        Cursor cursor = db.rawQuery("SELECT DISTINCT * FROM " + TABLE_NAME4,null);

        while(cursor.moveToNext()){
            list.add(new NewsModel(cursor.getInt(1),cursor.getString(2),cursor.getString(3),cursor.getString(4),cursor.getString(5),cursor.getString(6),cursor.getString(7),cursor.getString(8)));
        }
        return list;
    }

    public NewsModel getNewsFilterModel(int newsId){
        SQLiteDatabase db=this.getWritableDatabase();
        NewsModel item = null;

        Cursor cursor = db.rawQuery("SELECT * FROM "+TABLE_NAME4 + " WHERE " + NEWS_ID + " = ?",new String[]{ String.valueOf(newsId) });

        if( cursor.moveToFirst()){
            item= new NewsModel(cursor.getInt(1),cursor.getString(2),cursor.getString(3),cursor.getString(4),cursor.getString(5),cursor.getString(6),cursor.getString(7),cursor.getString(8));
        }
        return item;
    }

    public boolean updateNewsFilter(int newsId,String author, String title, String description,
                                    String url, String urlToImage, String publishedAt, String content){
        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues contentValues=new ContentValues();

        contentValues.put(NEWS_ID,newsId);
        contentValues.put(AUTHOR,author);
        contentValues.put(TITLE,title);
        contentValues.put(DESCRIPTION,description);
        contentValues.put(URL,url);
        contentValues.put(URL_TO_IMAGE,urlToImage);
        contentValues.put(PUBLISHED_AT,publishedAt);
        contentValues.put(CONTENT,content);

        db.update(TABLE_NAME4, contentValues,NEWS_ID + " = ?", new String[]{ String.valueOf(newsId) });
        return true;
    }

    public int deleteNewsFilter(int newsId){
        SQLiteDatabase db =this.getWritableDatabase();

        int result=db.delete(TABLE_NAME4,NEWS_ID+" = ?" , new String[]{ String.valueOf(newsId)});
        return result;
    }

    public void deleteAllNewsFilter(){
        SQLiteDatabase db =this.getWritableDatabase();
        db.execSQL("delete from "+ TABLE_NAME4);
    }

    public boolean insertFilterDay(int day,int dayLimit){
        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues contentValues=new ContentValues();

        contentValues.put(FILTER_DAY,day);
        contentValues.put(FILTER_LIMIT,dayLimit);

        long result= db.insert(TABLE_NAME5,null,contentValues); // -1-> failed else returns ID
        return result != -1;

    }

    public List<FilterDayModel> getAllFilterDays(){
        SQLiteDatabase db=this.getWritableDatabase();
        List<FilterDayModel> list=new ArrayList<>();
        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_NAME5,null);

        while(cursor.moveToNext()){
            list.add(new FilterDayModel(cursor.getInt(1),cursor.getInt(2)));
        }
        return list;
    }

    public FilterDayModel getSettingModel(int filterDay){
        SQLiteDatabase db=this.getWritableDatabase();
        FilterDayModel item = null;

        Cursor cursor = db.rawQuery("SELECT * FROM "+TABLE_NAME5 + " WHERE " + FILTER_DAY + " = ?",new String[]{ String.valueOf(filterDay) });

        if( cursor.moveToFirst()){
            item= new FilterDayModel(cursor.getInt(1),cursor.getInt(2));
        }
        return item;
    }

    public boolean updateFilterDays(int id,int filterDay){
        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues contentValues=new ContentValues();

        contentValues.put(FILTER_DAY,filterDay);

        db.update(TABLE_NAME5, contentValues,ID + " = ?", new String[]{ String.valueOf(id) });
        return true;
    }

    public int deleteFilterDays(int filterDay){
        SQLiteDatabase db =this.getWritableDatabase();

        return db.delete(TABLE_NAME5,FILTER_DAY+" = ?" , new String[]{ String.valueOf(filterDay)});
    }

    public void deleteAllFilterDays(){
        SQLiteDatabase db =this.getWritableDatabase();
        db.execSQL("delete from "+ TABLE_NAME5);
    }

}


