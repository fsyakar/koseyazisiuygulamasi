package com.example.newsdashboard.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.Toolbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.example.newsdashboard.R;
import com.example.newsdashboard.database.DatabaseHelper;
import com.example.newsdashboard.helper.RequestHelper;

import java.util.Calendar;
import java.util.Date;


public class MainActivity extends AppCompatActivity {

    //private ScrollView bgapp;
    //private Animation bganim;
    //private CardView cardView0;
    private CardView cardView0,cardView1,cardView2,cardView3,cardView4,cardView5;
    private Toolbar toolbar;
    private DatabaseHelper databaseHelper;
    private ImageButton imageButton;
    static private boolean isFirst=true;

    private static final int STORAGE_PERMISSION_CODE = 1;
    //private AlarmReceiver alarm;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        setContentView(R.layout.activity_main);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN );

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Ana Menü");


        imageButton = (ImageButton)findViewById(R.id.newspaper_gold_button);
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new RequestHelper(MainActivity.this,false).updateNews();
            }
        });


        /* Creates databases and plays for only first running */
        if(isFirst){
            /* Creates database and the new settings table */
            databaseHelper=new DatabaseHelper(this);

            /* Inserts default update interval time */
            if(databaseHelper.getAllSettings().isEmpty()){
                databaseHelper.insertSetting("0");
            }
            if(databaseHelper.getAllFilterDays().isEmpty()){
                databaseHelper.insertFilterDay(0,7);
            }

            /* Updates isFirst flag */
            isFirst=false;
        }

        cardView0 =(CardView) findViewById(R.id.card0);
        cardView0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this, NewsActivity.class);
                startActivity(intent);
            }
        });

        cardView1 =(CardView) findViewById(R.id.card1);
        cardView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this, FilterableNewsActivity.class);
                startActivity(intent);
            }
        });

        cardView2 =(CardView) findViewById(R.id.card2);
        cardView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this, MainSettingsActivity.class);
                startActivity(intent);
            }
        });

        cardView3 =(CardView) findViewById(R.id.card3);
        cardView3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this, InformationActivity.class);
                startActivity(intent);
            }
        });

    }

    @Override
    protected void onStop() {
        super.onStop();
    }

}