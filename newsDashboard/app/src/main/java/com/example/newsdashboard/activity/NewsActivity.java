package com.example.newsdashboard.activity;

import android.content.Intent;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.newsdashboard.R;
import com.example.newsdashboard.adapter.ListenNewsAdapter;
import com.example.newsdashboard.adapter.NewsAdapter;
import com.example.newsdashboard.database.DatabaseHelper;
import com.example.newsdashboard.model.NewsModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class NewsActivity extends AppCompatActivity {

    private RecyclerView recycler;
    private NewsAdapter clientAdapter;
    private Toolbar toolbar;
    private Button playAll;
    private Button pauseAll;
    private TextToSpeech tts;
    //private Intent intent;
    //private String id;


    private DatabaseHelper databaseHelper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_news);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN );

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Köşe Yazıları");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        tts = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if(status != TextToSpeech.ERROR) {
                    tts.setLanguage(new Locale("tr","TR"));
                }
            }
        });

        //intent=getIntent();
        //id=intent.getStringExtra("id");
        recycler=findViewById(R.id.recycler);
        recycler.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recycler.setLayoutManager(linearLayoutManager);

        playAll = (Button) findViewById(R.id.playAll);
        pauseAll = (Button) findViewById(R.id.pauseAll);

        databaseHelper=new DatabaseHelper(this);
        //List<newsModel> newsItems=databaseHelper.getAllNews();

        /*
        if(databaseHelper.getAllNews().size()==0){
            databaseHelper.insertNews(0,"Fatih Selim1","Başlık0","Açıklama",null,null,"2021","HER SEY BURDA");
            databaseHelper.insertNews(1,"Fatih Selim2","Başlık1","Açıklama",null,null,"2021","HER SEY BURDA");
            databaseHelper.insertNews(2,"Fatih Selim3","Başlık2","Açıklama",null,null,"2021","HER SEY BURDA");
        }*/

        clientAdapter = new NewsAdapter((ArrayList<NewsModel>)databaseHelper.getAllNews(), NewsActivity.this,new NewsAdapter.OnNewsItemClickListener() {
            @Override
            public void onItemClicker(int position) {
                Intent intent=new Intent(getApplicationContext(), NewsDetailActivity.class);
                intent.putExtra("id",position);
                intent.putExtra("type","normal");

                startActivity(intent);
            }
        });

        recycler.setAdapter(clientAdapter);

        playAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<NewsModel> downModels=databaseHelper.getAllNews();
                //StringBuilder builder=new StringBuilder();
                //Log.e("DOWNMODELS SIZE",downModels.size()+"");
                for(int i=0;i<downModels.size();++i){
                    NewsModel item=downModels.get(i);
                    String newColumnText = "Yeni Yazı; Yazar:"+item.getAuthor()+", Başlık:"+item.getTitle()+", Açıklama:"+item.getDescription()+"\n";
                    /*builder.append("Yeni Yazı;");
                    builder.append(new String("Yazar:"+item.getAuthor()+", Başlık:"+item.getTitle()+", Açıklama:"+item.getDescription()+"\n"));*/
                    int columnTextLength=newColumnText.length();
                    if(columnTextLength<3999){
                        tts.speak(newColumnText,TextToSpeech.QUEUE_ADD, null);
                    }
                    else{
                        int j=0;
                        while(columnTextLength>=3999){
                            tts.speak(newColumnText.substring(j,j+3999),TextToSpeech.QUEUE_ADD, null);
                            j+=3999;
                            columnTextLength-=3999;
                            if(columnTextLength<3999){
                                tts.speak(newColumnText.substring(j,j+columnTextLength),TextToSpeech.QUEUE_ADD, null);
                            }
                        }
                    }


                }
                //Log.e("String size",builder.toString().length()+"");
                //tts.speak(builder.toString(),TextToSpeech.QUEUE_ADD, null);
                Toast.makeText(v.getContext(),"Sırayla Oynatma Başlatıldı",Toast.LENGTH_SHORT).show();
            }
        });

        pauseAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(view. getContext(),"Sırayla Oynatma Durduruldu",Toast.LENGTH_SHORT).show();
                tts.stop();
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(NewsActivity.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        tts.stop();
    }
}
