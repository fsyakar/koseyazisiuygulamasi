package com.example.newsdashboard.model;

import java.util.Objects;

public class AuthorModel {
    private String name;

    public AuthorModel(){
        name="";
    }

    public AuthorModel(String name){
        this.name=name;
    }

    public String getAuthorName(){
        return this.name;
    }

    @Override
    public String toString() {
        return "authorModel{" +
                "name='" + name + '\'' +
                '}';
    }
}
