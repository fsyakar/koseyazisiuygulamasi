package com.example.newsdashboard.receiver;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.icu.util.Calendar;
import android.os.Build;
import android.widget.Toast;

import androidx.annotation.RequiresApi;

import com.example.newsdashboard.database.DatabaseHelper;
import com.example.newsdashboard.service.MakeAlarmTask;

public class AlarmReceiver extends BroadcastReceiver
{
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onReceive(Context context, Intent intent)
    {
        Intent in = new Intent(context, MakeAlarmTask.class);
        context.startService(in);
        setAlarm(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void setAlarm(Context context)
    {
        int updateIntervalMinute=Integer.parseInt(new DatabaseHelper(context).getAllSettings().get(0).getSetting());
        AlarmManager am =( AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        Intent i = new Intent(context, AlarmReceiver.class);
        PendingIntent pi = PendingIntent.getBroadcast(context, 0, i, 0);
        assert am != null;
        if(updateIntervalMinute!=0)
            am.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, (System.currentTimeMillis()/1000L +
                    (60L*updateIntervalMinute)) *1000L, pi); //Next alarm in 5s
        //Toast.makeText(context,"UPDATE INTERVAL SET:"+updateIntervalMinute,Toast.LENGTH_LONG).show();
        DatabaseHelper dbHelper =new DatabaseHelper(context);
        dbHelper.deleteAllSettings();
        dbHelper.insertSetting("1440");

    }


}
