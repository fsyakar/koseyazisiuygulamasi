package com.example.newsdashboard.service;

import android.content.Intent;
import android.widget.Toast;

import android.app.Service;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Process;
import android.util.Log;

import com.example.newsdashboard.helper.RequestHelper;

public class MakeAlarmTask extends Service {

    // This method run only one time. At the first time of service created and running
    @Override
    public void onCreate() {
        HandlerThread thread = new HandlerThread("ServiceStartArguments",
                Process.THREAD_PRIORITY_BACKGROUND);
        thread.start();
        Log.d("onCreate()", "After service created");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        //Here is the source of the TOASTS :D
        new RequestHelper(this,true).updateNews();
        Toast.makeText(this, "Köşe Yazısı Servisi Çalıştı", Toast.LENGTH_SHORT).show();

        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // We don't provide binding
        return null;
    }

}
