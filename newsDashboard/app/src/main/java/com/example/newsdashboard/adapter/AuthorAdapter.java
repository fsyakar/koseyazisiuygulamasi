package com.example.newsdashboard.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.newsdashboard.R;
import com.example.newsdashboard.database.DatabaseHelper;
import com.example.newsdashboard.model.AuthorModel;

import java.util.ArrayList;
import java.util.List;

public class AuthorAdapter extends RecyclerView.Adapter<AuthorAdapter.ViewHolder>{
    private Context mContext;
    private ArrayList<AuthorModel> downModels;
    private ImageButton deleteNewsButton;

    public AuthorAdapter(ArrayList<AuthorModel> downModels, Context mContext){
        this.mContext=mContext;
        this.downModels= (ArrayList<AuthorModel>) downModels;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(mContext).inflate(R.layout.author_row,parent,false);
        this.deleteNewsButton=view.findViewById(R.id.deleteAuthorButton);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder,final int position) {
        final AuthorModel clientModel= downModels.get(position);
        holder.name.setText(clientModel.getAuthorName());
        deleteNewsButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                int adapterPosition=holder.getAdapterPosition();
                Log.i("AuthorAdapter",adapterPosition+"");
                int res= new DatabaseHelper(mContext).deleteAuthor(downModels.get(adapterPosition).getAuthorName());
                downModels.remove(adapterPosition);
                Log.i("AuthorAdapter",downModels.toString());
                notifyItemRemoved(adapterPosition);

                if(res>=1){
                    Toast.makeText(mContext,"Yazar Başarıyla Silindi!",Toast.LENGTH_SHORT).show();
                }
                else{
                    Toast.makeText(mContext,"Yazar Silinemedi Veya Yok!",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void updateList (List<AuthorModel> items) {
        if (items != null && items.size() > 0) {
            downModels.clear();
            downModels.addAll(items);
            notifyDataSetChanged();
        }
    }

    public class ViewHolder extends  RecyclerView.ViewHolder{
        TextView name;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            name=itemView.findViewById(R.id.authorName);
        }
    }

    @Override
    public int getItemCount() {
        return downModels.size();
    }
}
