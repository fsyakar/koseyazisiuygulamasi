package com.example.newsdashboard.helper;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.example.newsdashboard.database.DatabaseHelper;
import com.example.newsdashboard.model.AuthorModel;
import com.example.newsdashboard.model.FilterDayModel;
import com.example.newsdashboard.model.NewsModel;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.util.List;

public class RequestHelper {
    private DatabaseHelper databaseHelper;
    private Context context;
    private Boolean isService;

    public RequestHelper(Context context, Boolean isService){
        this.databaseHelper=new DatabaseHelper(context);
        this.context=context;
        this.isService=isService;
    }

    public void updateNews(){
        if(this.getConnectivityStatus(context)){
            databaseHelper.deleteAllNews();
            FilterDayModel dayModel=databaseHelper.getAllFilterDays().get(0);
            int filterDay=dayModel.getFilterDay();
            int filterDayLimit=dayModel.getFilterLimit();
            Toast.makeText(context,"Güncelleme Gün Sayacı:"+filterDay+ " Limit: "+filterDayLimit,Toast.LENGTH_SHORT).show();
            databaseHelper.deleteAllFilterDays();
            if(filterDay>=filterDayLimit){
                databaseHelper.insertFilterDay(0,filterDayLimit);
                databaseHelper.deleteAllNewsFilter();
            }
            else if(isService){
                databaseHelper.insertFilterDay(filterDay+1,filterDayLimit);
            }
            else{
                databaseHelper.insertFilterDay(filterDay,filterDayLimit);
            }
            List<AuthorModel> authorList = databaseHelper.getAllAuthors();
            for(int i=0;i<authorList.size();++i){
                try {
                    this.run(authorList.get(i).getAuthorName());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        else{
            Toast.makeText(context,"İnternet Erişimi Yok!",Toast.LENGTH_LONG).show();
        }
    }

    public String findCharSequenceInString(String charSequence,String text,int mod){
        String templateCharSequence = null;
        if(mod==0){
            int i=0;
            do{
                templateCharSequence="";
                i=text.indexOf(charSequence,i);

                for(;text.charAt(i)!='"'&& i<text.length(); ++i){
                    templateCharSequence+=text.charAt(i);
                }
            }
            while(templateCharSequence.equals(charSequence) && i<text.length());
        }
        else if(mod==1){
            int i=0;
            do{
                templateCharSequence="";
                i=text.indexOf(charSequence,i);

                for(;text.charAt(i)!='"'&& i<text.length(); ++i){
                    templateCharSequence+=text.charAt(i);
                }
            }
            while(templateCharSequence.equals(charSequence) && i<text.length() /*|| !templateCharSequence.matches(charSequence+"[0-9][a-zA-Z]*")*/);
        }

        return templateCharSequence;
    }


    public void run(String author) throws IOException {
        new SendRequest().execute(author);
    }

    public boolean getConnectivityStatus(Context context) {
        boolean connected = false;
        ConnectivityManager connectivityManager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            //we are connected to a network
            connected = true;
        }
        else
            connected = false;
        return connected;
    }

    private class SendRequest extends AsyncTask<String, Void, String> {

        private ProgressDialog dialog;
        private String author;
        private String newsLink;

        protected String doInBackground(String... strings) {
            author=strings[0];
            OkHttpClient client = new OkHttpClient();
            newsLink = "";

            try{
                if(author.contains("Ertuğrul Özkök") || author.contains("Ahmet Hakan")){
                    String authorLinkName="";
                    if(author.contains("Ertuğrul Özkök")){
                        authorLinkName="ertugrul-ozkok";
                    }
                    else if(author.contains("Ahmet Hakan")){
                        authorLinkName="ahmet-hakan";
                    }

                    Request request = new Request.Builder().url("https://www.hurriyet.com.tr/yazarlar/"+authorLinkName+"/").build();
                    Response response = client.newCall(request).execute();
                    if(response.isSuccessful()){
                        String rp = response.body().string();
                        newsLink=findCharSequenceInString("yazarlar/"+authorLinkName+"/",rp,0);
                        newsLink="https://www.hurriyet.com.tr/"+newsLink;
                    }
                    else {
                        System.out.print("Inner request failed with status");
                    }

                }
                else if(author.contains("Taha Akyol") || author.contains("Mehmet Ocaktan") || author.contains("Yusuf Ziya Cömert") || author.contains("Elif Çakır")){
                    String authorLinkName="";
                    if(author.contains("Taha Akyol")){
                        authorLinkName="taha-akyol";
                    }
                    else if(author.contains("Mehmet Ocaktan")){
                        authorLinkName="mehmet-ocaktan";
                    }
                    else if(author.contains("Yusuf Ziya Cömert")){
                        authorLinkName="yusuf-ziya-comert";
                    }
                    else if(author.contains("Elif Çakır")){
                        authorLinkName="elif-cakir";
                    }

                    Request request = new Request.Builder().url("https://www.karar.com/yazarlar/"+authorLinkName).build();
                    Response response = client.newCall(request).execute();
                    if(response.isSuccessful()){
                        String rp = response.body().string();
                        newsLink=findCharSequenceInString("/yazarlar/"+authorLinkName+"/",rp,0);
                        newsLink="https://www.karar.com"+newsLink;
                    }
                    else {
                        System.out.print("Inner request failed with status");
                    }
                }
                else if(author.contains("Mehmet Barlas") || author.contains("Salih Tuna") || author.contains("Engin Ardıç") || author.contains("Kerem Alkin") || author.contains("Sait Gürsoy") || author.contains("Hıncal Uluç") || author.contains("Hilal Kaplan") || author.contains("Halit Yerebakan")){
                    String authorLinkName="";
                    if(author.contains("Mehmet Barlas")){
                        authorLinkName="barlas";
                    }
                    else if(author.contains("Salih Tuna")){
                        authorLinkName="salih-tuna";
                    }
                    else if(author.contains("Engin Ardıç")){
                        authorLinkName="ardic";
                    }
                    else if(author.contains("Kerem Alkin")){
                        authorLinkName="kerem-alkin";
                    }
                    else if(author.contains("Sait Gürsoy")){
                        authorLinkName="gursoy";
                    }
                    else if(author.contains("Hıncal Uluç")){
                        authorLinkName="uluc";
                    }
                    else if(author.contains("Hilal Kaplan")){
                        authorLinkName="hilalkaplan";
                    }
                    else if(author.contains("Halit Yerebakan")){
                        authorLinkName="halit-yerebakan";
                    }

                    Request request = new Request.Builder().url("https://www.sabah.com.tr/yazarlar/"+authorLinkName).build();
                    Response response = client.newCall(request).execute();
                    if(response.isSuccessful()){
                        String rp = response.body().string();
                        newsLink=findCharSequenceInString("yazarlar/"+authorLinkName+"/2",rp,0);
                        newsLink="https://www.sabah.com.tr/"+newsLink;
                    }
                    else {
                        System.out.print("Inner request failed with status");
                    }
                }
                else if(author.contains("Yılmaz Özdil") || author.contains("Uğur Dündar") || author.contains("Emin Çölaşan") || author.contains("Rahmi Turan") || author.contains("Bekir Coşkun") || author.contains("Necati Doğru") || author.contains("Saygı Öztürk") || author.contains("Soner Yalçın")){
                    String authorLinkName="";
                    if(author.contains("Uğur Dündar")){
                        authorLinkName="ugur-dundar";
                    }
                    else if(author.contains("Yılmaz Özdil")){
                        authorLinkName="yilmaz-ozdil";
                    }
                    else if(author.contains("Emin Çölaşan")){
                        authorLinkName="emin-colasan";
                    }
                    else if(author.contains("Rahmi Turan")){
                        authorLinkName="rahmi-turan";
                    }
                    else if(author.contains("Bekir Coşkun")){
                        authorLinkName="bekir-coskun";
                    }
                    else if(author.contains("Necati Doğru")){
                        authorLinkName="necati-dogru";
                    }
                    else if(author.contains("Saygı Öztürk")){
                        authorLinkName="saygi-ozturk";
                    }
                    else if(author.contains("Soner Yalçın")){
                        authorLinkName="soner-yalcin";
                    }

                    Request request = new Request.Builder().url("https://www.sozcu.com.tr/2021/yazarlar/"+authorLinkName).build();
                    Response response = client.newCall(request).execute();

                    if(response.isSuccessful()){
                        String rp = response.body().string();
                        newsLink=findCharSequenceInString("https://www.sozcu.com.tr/2021/yazarlar/"+authorLinkName+"/",rp,0);
                    }
                    else {
                        System.out.print("Inner request failed with status");
                    }
                }
                else if(author.contains("Fatih Altaylı") || author.contains("Murat Bardakçı") || author.contains("Nagehan Alçı") || author.contains("Kübra Par") || author.contains("Sevilay Yılman") || author.contains("Nihal Bengisu Karaca")){
                    String authorLinkName="";
                    if(author.contains("Fatih Altaylı")){
                        authorLinkName="fatih-altayli-1001";
                    }
                    else if(author.contains("Murat Bardakçı")){
                        authorLinkName="murat-bardakci";
                    }
                    else if(author.contains("Nagehan Alçı")){
                        authorLinkName="nagehan-alci";
                    }
                    else if(author.contains("Kübra Par")){
                        authorLinkName="kubra-par-2561";
                    }
                    else if(author.contains("Sevilay Yılman")){
                        authorLinkName="sevilay-yilman-2383";
                    }
                    else if(author.contains("Nihal Bengisu Karaca")){
                        authorLinkName="nihal-bengisu-karaca";
                    }

                    Request request = new Request.Builder().url("https://www.haberturk.com/htyazar/"+authorLinkName).build();
                    Response response = client.newCall(request).execute();

                    if(response.isSuccessful()){
                        String rp = response.body().string();
                        newsLink=findCharSequenceInString("/yazarlar/"+authorLinkName+"/",rp,0);
                        newsLink="https://www.haberturk.com"+newsLink;
                    }
                    else {
                        System.out.print("Inner request failed with status");
                    }
                }
                else if(author.contains("Abdurrahman Dilipak") || author.contains("Ahmet Gülümseyen") || author.contains("Akif Bedir") || author.contains("Ali Karahasanoğlu") || author.contains("Ayhan Demir") || author.contains("Hüseyin Öztürk") || author.contains("Mustafa Çelik") || author.contains("Vehbi Kara")){
                    String authorLinkName="";
                    if(author.contains("Abdurrahman Dilipak")){
                        authorLinkName="abdurrahman-dilipak";
                    }
                    else if(author.contains("Ahmet Gülümseyen")){
                        authorLinkName="ahmet-gulumseyen";
                    }
                    else if(author.contains("Akif Bedir")){
                        authorLinkName="akif-bedir";
                    }
                    else if(author.contains("Ali Karahasanoğlu")){
                        authorLinkName="ali-karahasanoglu";
                    }
                    else if(author.contains("Ayhan Demir")){
                        authorLinkName="ayhan-demir";
                    }
                    else if(author.contains("Hüseyin Öztürk")){
                        authorLinkName="huseyin-ozturk";
                    }
                    else if(author.contains("Mustafa Çelik")){
                        authorLinkName="mustafa-celik";
                    }
                    else if(author.contains("Vehbi Kara")){
                        authorLinkName="vehbi-kara";
                    }

                    Request request = new Request.Builder().url("https://www.yeniakit.com.tr/yazarlar/"+authorLinkName).build();
                    Response response = client.newCall(request).execute();

                    if(response.isSuccessful()){
                        String rp = response.body().string();
                        newsLink=findCharSequenceInString("https://www.yeniakit.com.tr/yazarlar/"+authorLinkName+"/",rp,0);
                    }
                    else {
                        System.out.print("Inner request failed with status");
                    }
                }
                else if(author.contains("Tamer Korkmaz") || author.contains("Nedret Ersanel") || author.contains("Bülent Orakoğlu") || author.contains("Ergün Yıldırım") || author.contains("Mehmet Acet") || author.contains("Taha Kılınç") || author.contains("Mustafa Kutlu") || author.contains("Yaşar Süngü") || author.contains("Yasin Aktay") || author.contains("Ahmet Ulusoy")){
                    String authorLinkName="";
                    if(author.contains("Tamer Korkmaz")){
                        authorLinkName="tamer-korkmaz";
                    }
                    else if(author.contains("Nedret Ersanel")){
                        authorLinkName="nedret-ersanel";
                    }
                    else if(author.contains("Bülent Orakoğlu")){
                        authorLinkName="bulent-orakoglu";
                    }
                    else if(author.contains("Ergün Yıldırım")){
                        authorLinkName="ergun-yildirim";
                    }
                    else if(author.contains("Mehmet Acet")){
                        authorLinkName="mehmet-acet";
                    }
                    else if(author.contains("Taha Kılınç")){
                        authorLinkName="taha-kilinc";
                    }
                    else if(author.contains("Mustafa Kutlu")){
                        authorLinkName="mustafa-kutlu";
                    }
                    else if(author.contains("Yaşar Süngü")){
                        authorLinkName="yasar-sungu";
                    }
                    else if(author.contains("Yasin Aktay")){
                        authorLinkName="yasin-aktay";
                    }
                    else if(author.contains("Ahmet Ulusoy")) {
                        authorLinkName = "ahmet-ulusoy";
                    }

                    Request request = new Request.Builder().url("https://www.yenisafak.com/yazarlar/"+authorLinkName).build();
                    Response response = client.newCall(request).execute();
                    if(response.isSuccessful()){
                        String rp = response.body().string();
                        newsLink=findCharSequenceInString("/yazarlar/"+authorLinkName+"/",rp,0);
                        newsLink="https://www.yenisafak.com"+newsLink;
                    }
                    else {
                        System.out.print("Inner request failed with status");
                    }
                }
                else if(author.contains("Mustafa Balbay") || author.contains("Adnan Dinçer") || author.contains("Evin İlyasoğlu") || author.contains("Mine Söğüt") || author.contains("Öztin Akgüç") || author.contains("Deniz Yıldırım") || author.contains("Barış Doster") || author.contains("Elçin Poyrazlar")){
                    String authorLinkName="";
                    if(author.contains("Mustafa Balbay")){
                        authorLinkName="mustafa-balbay";
                    }
                    else if(author.contains("Adnan Dinçer")){
                        authorLinkName="adnan-dincer";
                    }
                    else if(author.contains("Evin İlyasoğlu")){
                        authorLinkName="evin-ilyasoglu";
                    }
                    else if(author.contains("Mine Söğüt")){
                        authorLinkName="mine-sogut";
                    }
                    else if(author.contains("Öztin Akgüç")){
                        authorLinkName="oztin-akguc";
                    }
                    else if(author.contains("Deniz Yıldırım")){
                        authorLinkName="deniz-yildirim";
                    }
                    else if(author.contains("Barış Doster")){
                        authorLinkName="baris-doster";
                    }
                    else if(author.contains("Elçin Poyrazlar")){
                        authorLinkName="elcin-poyrazlar";
                    }

                    Request request = new Request.Builder().url("https://www.cumhuriyet.com.tr/yazarlar/"+authorLinkName+"/").build();
                    Response response = client.newCall(request).execute();
                    if(response.isSuccessful()){
                        String rp = response.body().string();
                        newsLink=findCharSequenceInString("/yazarlar/"+authorLinkName+"/",rp,0);
                        newsLink="https://www.cumhuriyet.com.tr"+newsLink;
                    }
                    else {
                        System.out.print("Inner request failed with status");
                    }

                }

                OkHttpClient client2 = new OkHttpClient();
                Request request = new Request.Builder()
                    .url("https://extractnews.herokuapp.com/v0/article?url="+newsLink)
                    .get()
                    //.addHeader("x-rapidapi-key", "56bf1e2b75msh317f45f1c14a700p116055jsnadb37a244e06")
                    //.addHeader("x-rapidapi-host", "extract-news.p.rapidapi.com")
                    .build();

                Response response = client2.newCall(request).execute();
                return response.body().string();
            }
            catch (java.io.IOException e){
                Log.e("doInBackground",e.getMessage());
                return "{}";
            }
        }

        protected void onPostExecute(String result) {
            JSONObject jsonObject= null;
            try {
                jsonObject = new JSONObject(result);
                parseJson(jsonObject);
            } catch (JSONException e) {
                Log.e("onPostExecute",e.getMessage());
                Log.e("onPostExecute",result);
            }

            if(!isService){
                if( this.dialog.isShowing()) {
                    this.dialog.dismiss();
                }
            }
        }

        protected void onPreExecute()
        {
            // show progress bar or something
            if(!isService) {
                this.dialog = new ProgressDialog(context);
                this.dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                this.dialog.setMessage("Haberler Güncelleniyor...");
                this.dialog.show();
            }
        }

        protected void onProgressUpdate(){
            super.onProgressUpdate();
        }

        private void parseJson(JSONObject jsonObject) throws JSONException {
            int loopSize=5,startIndex,startIndexFilter;
            List<NewsModel> newsFilterList;
            JSONObject mJsonProperty = jsonObject.getJSONObject("article");

            newsFilterList=databaseHelper.getAllNewsFilter();
            startIndex=databaseHelper.getAllNews().size();
            startIndexFilter=newsFilterList.size();


            String title = mJsonProperty.getString("title");
            String imageURL;
            try{
                imageURL = mJsonProperty.getString("top_image");
            }
            catch (Exception e){
                imageURL="https://www.udg.az/wp-content/themes/fitspiration/images/blank.jpg";
            }
            String description = mJsonProperty.getString("text");
            String content = mJsonProperty.getString("text");
            String url = this.newsLink;//mJsonProperty.getString("source_url");
            String author = this.author;//mJsonProperty.getString("authors");
            String publishedAt = mJsonProperty.getString("published");

            //Log.i("Response:",title+" "+imageURL+" "+description+" "+url+" "+author+" "+publishedAt);
            boolean notExist=true;
            for(NewsModel model:newsFilterList){
                if(model.getUrl().equals(url)){
                    notExist=false;
                    break;
                }
            }
            if(notExist){
                databaseHelper.insertNewsFilter(startIndexFilter,author,title,description,url,imageURL,publishedAt,url);
            }

            databaseHelper.insertNews(startIndex,author,title,description,url,imageURL,publishedAt,url);


        }
        /*private void parseJson(JSONObject jsonObject) throws JSONException {
            int loopSize=5,startIndex,startIndexFilter;
            List<NewsModel> newsFilterList;
            JSONArray mJsonArrayProperty = jsonObject.getJSONArray("value");
            if(mJsonArrayProperty.length()<loopSize){
                loopSize=mJsonArrayProperty.length();
            }
            newsFilterList=databaseHelper.getAllNewsFilter();
            startIndex=databaseHelper.getAllNews().size();
            startIndexFilter=newsFilterList.size();
            for (int i = 0; i < loopSize; i++) {
                JSONObject mJsonObjectProperty = mJsonArrayProperty.getJSONObject(i);

                String title = mJsonObjectProperty.getString("name");
                String imageURL;
                try{
                    imageURL = new JSONObject(mJsonObjectProperty.getString("image")).getString("contentUrl");
                }
                catch (Exception e){
                    imageURL="https://www.udg.az/wp-content/themes/fitspiration/images/blank.jpg";
                }
                String description = mJsonObjectProperty.getString("description");
                String content = mJsonObjectProperty.getString("description");
                String url = mJsonObjectProperty.getString("url");
                String author = new JSONArray(mJsonObjectProperty.getString("provider")).getJSONObject(0).getString("name");
                String publishedAt = mJsonObjectProperty.getString("datePublished");

                //Log.i("Response:",title+" "+imageURL+" "+description+" "+url+" "+author+" "+publishedAt);
                boolean notExist=true;
                for(NewsModel model:newsFilterList){
                    if(model.getUrl().equals(url)){
                        notExist=false;
                        break;
                    }
                }
                if(notExist){
                    databaseHelper.insertNewsFilter(startIndexFilter+i,author,title,description,url,imageURL,publishedAt.substring(0,16),url);
                }

                databaseHelper.insertNews(startIndex+i,author,title,description,url,imageURL,publishedAt.substring(0,16),url);

            }
        }*/
    }

}

