package com.example.newsdashboard.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.newsdashboard.R;
import com.example.newsdashboard.holder.NewsViewHolder;
import com.example.newsdashboard.model.NewsModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class NewsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable {

    private Context mContext;
    private ArrayList<NewsModel> downModels;
    private ArrayList<NewsModel> downModelsFiltered;
    private OnNewsItemClickListener listener;

    public NewsAdapter(List<NewsModel> downModels, Context mContext, OnNewsItemClickListener listener){
        this.downModels= (ArrayList<NewsModel>)downModels;
        this.downModelsFiltered= (ArrayList<NewsModel>)downModels;
        this.mContext=mContext;
        this.listener=listener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.news_row,parent,false);
        return new NewsViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        NewsViewHolder viewHolder = (NewsViewHolder) holder;
        NewsModel item=downModels.get(position);
        Picasso.get().load(item.getUrlToImage()).into(viewHolder.image);
        viewHolder.title.setText(item.getTitle());
        viewHolder.text.setText(item.getAuthor());

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                listener.onItemClicker(item.getId());
            }
        });
    }

    @Override
    public int getItemCount() {
        return downModels.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    downModels = downModelsFiltered;
                } else {
                    ArrayList<NewsModel> filteredList = new ArrayList<>();
                    for (NewsModel row : downModelsFiltered) {
                        String willSearched= row.getAuthor()+' '+row.getContent()+' '+ row.getDescription()+' '+row.getTitle();
                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (willSearched.toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    downModels = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = downModels;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                downModels = (ArrayList<NewsModel>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public interface OnNewsItemClickListener{
        public void onItemClicker(int position);
    }

    public void updateList (List<NewsModel> items) {
        if (items != null && items.size() > 0) {
            downModels.clear();
            downModels.addAll(items);
            notifyDataSetChanged();
        }
    }
}
