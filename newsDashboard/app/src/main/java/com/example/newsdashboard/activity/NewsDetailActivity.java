package com.example.newsdashboard.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.newsdashboard.R;
import com.example.newsdashboard.database.DatabaseHelper;
import com.example.newsdashboard.model.NewsModel;
import com.squareup.picasso.Picasso;

import java.util.Locale;

public class NewsDetailActivity extends AppCompatActivity {
    private ImageView newsImage;
    private TextView newsTitle,newsSubtitle,newsTime,newsPublisher,newsText;
    //TextView newsImageDescription;
    //private Button detailButton;
    private Toolbar toolbar;
    private int id;
    private String type;
    private ImageButton playButton;
    private ImageButton pauseButton;
    private TextToSpeech tts;
    private NewsModel currentNews;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_detail);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN );

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Köşe Yazısı");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        playButton = findViewById(R.id.playButton);
        pauseButton = findViewById(R.id.pauseButton);

        tts = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if(status != TextToSpeech.ERROR) {
                    tts.setLanguage(new Locale("tr","TR"));
                }
            }
        });



        newsImage = findViewById(R.id.newsImage);
        newsTitle = findViewById(R.id.newsTitle);
        newsSubtitle = findViewById(R.id.newsSubtitle);
        //newsImageDescription = findViewById(R.id.newsImageDescription);
        newsTime = findViewById(R.id.newsTime);
        newsPublisher = findViewById(R.id.newsPublisher);
        newsText = findViewById(R.id.newsText);
        //detailButton = findViewById(R.id.detailButton);
        //detailButton.setText("Detay İçin Tıklayınız.");

        Intent intent = getIntent();
        if(intent!=null){
            id= intent.getIntExtra("id",0);
            type=intent.getStringExtra("type");
            DatabaseHelper databaseHelper = new DatabaseHelper(this);
            NewsModel item=null;
            if(type.equals("normal")){
                item=databaseHelper.getNewsModel(id);
                currentNews=item;
            }
            else if(type.equals("filterable")){
                item=databaseHelper.getNewsFilterModel(id);
                currentNews=item;
            }

            Picasso.get().load(item.getUrlToImage()).into(newsImage);
            //newsImageDescription.setText("Resim Açıklaması"); // Burası düzenlenicek
            newsTitle.setText(item.getTitle());
            newsSubtitle.setText(item.getDescription());
            newsTime.setText(item.getPublishedAt());
            newsPublisher.setText(item.getAuthor());
            newsText.setText(item.getContent());
        }

        playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String listenText="Yazar:"+currentNews.getAuthor()+", Başlık:"+currentNews.getTitle()+", Açıklama:"+currentNews.getDescription();
                int listenTextLength=listenText.length();
                Toast.makeText(view. getContext(), "Oynat Butonuna Basıldı", Toast. LENGTH_SHORT).show();
                //tts.speak(listenText,TextToSpeech.QUEUE_FLUSH, null);
                int j=0;
                if(listenTextLength<3999){
                    tts.speak(listenText,TextToSpeech.QUEUE_FLUSH, null);
                }
                else{
                    while(listenTextLength>=3999){
                        tts.speak(listenText.substring(j,j+3999),TextToSpeech.QUEUE_ADD, null);
                        j+=3999;
                        listenTextLength-=3999;
                        if(listenTextLength<3999){
                            tts.speak(listenText.substring(j,j+listenTextLength),TextToSpeech.QUEUE_ADD, null);
                        }
                    }
                }
            }
        });

        pauseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(view. getContext(), "Durdur Butonuna Basıldı", Toast. LENGTH_SHORT).show();
                tts.stop();
            }
        });

        /*detailButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getApplicationContext(), NewsLinkActivity.class);
                intent.putExtra("link",newsText.getText());
                intent.putExtra("id",id);
                intent.putExtra("type",type);
                startActivity(intent);
            }
        });*/

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                Intent intent=null;
                if(type.equals("filterable")){
                    intent=new Intent(NewsDetailActivity.this, FilterableNewsActivity.class);
                }
                else if(type.equals("normal")){
                    intent=new Intent(NewsDetailActivity.this, NewsActivity.class);
                }
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        tts.stop();
    }
}