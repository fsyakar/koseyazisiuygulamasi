//todo: BU CLASSIN ISMI DEGISTIRILECEK
package com.example.newsdashboard.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.newsdashboard.R;
import com.example.newsdashboard.adapter.AuthorAdapter;
import com.example.newsdashboard.database.DatabaseHelper;
import com.example.newsdashboard.model.AuthorModel;
//import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import java.util.ArrayList;

import gr.escsoft.michaelprimez.searchablespinner.SearchableSpinner;
import gr.escsoft.michaelprimez.searchablespinner.interfaces.OnItemSelectedListener;

public class AuthorSettingsActivity extends AppCompatActivity {

    private DatabaseHelper databaseHelperAuthor;
    private ImageButton addButton;
    private ImageButton deleteButton;
    //private EditText text_message;
    private SearchableSpinner author;
    private Toolbar toolbar;

    //New added
    private RecyclerView recycler;
    private AuthorAdapter clientAdapter;
    private ArrayList<AuthorModel> clientModels;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_author_settings);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN );

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Yazarlar");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        //addButton = findViewById(R.id.addButton);

        //deleteButton = findViewById(R.id.deleteButton);

        author = findViewById(R.id.authorNameLogin);
        ArrayAdapter<String> authorAdapter = new ArrayAdapter<String>(AuthorSettingsActivity.this,android.R.layout.simple_list_item_1,getResources().getStringArray(R.array.authors));
        authorAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        author.setAdapter(authorAdapter);
        author.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(View view, int position, long id) {
                //Toast.makeText(authorAddActivity.this,"Seçildi! "+position+" "+author.getSelectedItem().toString(),Toast.LENGTH_LONG).show();
                String msg = author.getSelectedItem().toString();
                if(msg.toString().length()==0){
                    Toast.makeText(AuthorSettingsActivity.this,"Yazar Kısmı Boş Bırakılamaz!",Toast.LENGTH_SHORT).show();
                    return;
                }
                databaseHelperAuthor = new DatabaseHelper(AuthorSettingsActivity.this);
                if(databaseHelperAuthor.insertAuthor(msg)){
                    Toast.makeText(AuthorSettingsActivity.this,"Yazar Başarıyla Eklendi!",Toast.LENGTH_SHORT).show();
                }
                else{
                    Toast.makeText(AuthorSettingsActivity.this,"Yazar Eklenemedi Veya Zaten Mevcut!",Toast.LENGTH_SHORT).show();
                }
                clientAdapter.updateList((ArrayList<AuthorModel>)databaseHelperAuthor.getAllAuthors());
            }

            @Override
            public void onNothingSelected() {
                Toast.makeText(AuthorSettingsActivity.this,"Seçim Yapılmadı! ",Toast.LENGTH_SHORT).show();
            }
        });

        //intent=getIntent();
        //id=intent.getStringExtra("id");
        recycler=findViewById(R.id.recycler);
        recycler.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recycler.setLayoutManager(linearLayoutManager);
        databaseHelperAuthor=new DatabaseHelper(this);

        clientAdapter = new AuthorAdapter((ArrayList<AuthorModel>)databaseHelperAuthor.getAllAuthors(), AuthorSettingsActivity.this);
        recycler.setAdapter(clientAdapter);

        /*addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String msg = author.getSelectedItem().toString();
                if(msg.toString().length()==0){
                    Toast.makeText(authorAddActivity.this,"Yazar Kısmı Boş Bırakılamaz!",Toast.LENGTH_LONG).show();
                    return;
                }
                databaseHelperAuthor = new DatabaseHelper(authorAddActivity.this);
                if(databaseHelperAuthor.insertAuthor(msg)){
                    Toast.makeText(authorAddActivity.this,"Yazar Başarıyla Eklendi!",Toast.LENGTH_LONG).show();
                }
                else{
                    Toast.makeText(authorAddActivity.this,"Yazar Eklenemedi Veya Zaten Mevcut!",Toast.LENGTH_LONG).show();
                }
            }
        });*/

        /*deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String msg = author.getSelectedItem().toString();
                if(msg.toString().length()==0){
                    Toast.makeText(authorAddActivity.this,"Yazar Kısmı Boş Bırakılamaz!",Toast.LENGTH_LONG).show();
                    return;
                }
                databaseHelperAuthor = new DatabaseHelper(authorAddActivity.this);
                if(databaseHelperAuthor.deleteAuthor(msg)>=1){
                    Toast.makeText(authorAddActivity.this,"Yazar Başarıyla Silindi!",Toast.LENGTH_LONG).show();
                }
                else{
                    Toast.makeText(authorAddActivity.this,"Yazar Silinemedi Veya Yok!",Toast.LENGTH_LONG).show();
                }
            }
        });*/

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(AuthorSettingsActivity.this, MainSettingsActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
