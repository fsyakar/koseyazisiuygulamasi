package com.example.newsdashboard.activity;

import android.content.Intent;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.newsdashboard.R;
import com.example.newsdashboard.adapter.ListenNewsAdapter;
import com.example.newsdashboard.database.DatabaseHelper;
import com.example.newsdashboard.model.NewsModel;

import java.util.ArrayList;
import java.util.Locale;
import java.util.Objects;

public class ListenNewsActivity extends AppCompatActivity {

    private RecyclerView recycler;
    private ListenNewsAdapter clientAdapter;
    private Toolbar toolbar;
    private Button playAll;
    private Button pauseAll;
    //private Intent intent;
    //private String id;

    private DatabaseHelper databaseHelper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_listen_news);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN );

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setTitle("Köşe Yazılarını Dinleme");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        //intent=getIntent();
        //id=intent.getStringExtra("id");
        recycler=findViewById(R.id.recycler);
        recycler.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recycler.setLayoutManager(linearLayoutManager);

        databaseHelper=new DatabaseHelper(this);
        //List<newsModel> newsItems=databaseHelper.getAllNews();

        /*
        if(databaseHelper.getAllNews().size()==0){
            databaseHelper.insertNews(0,"Fatih Selim1","Başlık0","Açıklama",null,null,"2021","HER SEY BURDA");
            databaseHelper.insertNews(1,"Fatih Selim2","Başlık1","Açıklama",null,null,"2021","HER SEY BURDA");
            databaseHelper.insertNews(2,"Fatih Selim3","Başlık2","Açıklama",null,null,"2021","HER SEY BURDA");
        }*/
        playAll = (Button) findViewById(R.id.playAll);
        pauseAll = (Button) findViewById(R.id.pauseAll);

        clientAdapter = new ListenNewsAdapter((ArrayList<NewsModel>)databaseHelper.getAllNews(), ListenNewsActivity.this,playAll,pauseAll);

        recycler.setAdapter(clientAdapter);

        /*playAll = (Button) findViewById(R.id.playAll);
        playAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(listenNewsActivity.this,"Play All Clicked",Toast.LENGTH_SHORT).show();
            }
        });*/

        /*pauseAll = (Button) findViewById(R.id.pauseAll);
        pauseAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(listenNewsActivity.this,"Pause All Clicked",Toast.LENGTH_SHORT).show();
            }
        });*/

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(ListenNewsActivity.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        clientAdapter.stopAll();
    }
}
