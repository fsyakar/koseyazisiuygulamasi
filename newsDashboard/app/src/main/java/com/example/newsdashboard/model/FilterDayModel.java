package com.example.newsdashboard.model;

public class FilterDayModel {
    private int filterDay;
    private int filterLimit;

    public FilterDayModel(){
        filterDay=0;
        filterLimit=7;
    }

    public FilterDayModel(int filterDay,int filterLimit){
        this.filterDay=filterDay;
        this.filterLimit=filterLimit;
    }

    public int getFilterDay(){
        return this.filterDay;
    }

    public int getFilterLimit(){
        return this.filterLimit;
    }


    @Override
    public String toString() {
        return "FilterDayModel{" +
                "filterDay=" + filterDay + "filterLimit" + filterLimit +
                '}';
    }
}
