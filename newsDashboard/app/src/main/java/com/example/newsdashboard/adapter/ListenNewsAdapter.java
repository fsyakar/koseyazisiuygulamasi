package com.example.newsdashboard.adapter;

import android.content.Context;
import android.speech.tts.TextToSpeech;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.newsdashboard.R;
import com.example.newsdashboard.holder.ListenNewsViewHolder;
import com.example.newsdashboard.model.NewsModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class ListenNewsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private ArrayList<NewsModel> downModels;
    private TextToSpeech tts;
    private Button playAll,pauseAll;

    public ListenNewsAdapter(List<NewsModel> downModels, Context mContext, Button playAll, Button pauseAll){
        this.downModels= (ArrayList<NewsModel>)downModels;
        this.mContext=mContext;
        this.playAll=playAll;
        this.pauseAll=pauseAll;
        tts=new TextToSpeech(mContext, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if(status != TextToSpeech.ERROR) {
                    tts.setLanguage(new Locale("tr","TR"));
                }
            }
        });
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.listen_news_row,parent,false);
        return new ListenNewsViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        ListenNewsViewHolder viewHolder = (ListenNewsViewHolder) holder;
        NewsModel item=downModels.get(position);
        viewHolder.title.setText(item.getTitle());
        viewHolder.author.setText(item.getAuthor());
        viewHolder.play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String listenText="Yazar:"+item.getAuthor()+", Başlık:"+item.getTitle()+", Açıklama:"+item.getDescription();
                Toast.makeText(view. getContext(), "Play Button Clicked", Toast. LENGTH_SHORT).show();
                tts.speak(listenText,TextToSpeech.QUEUE_FLUSH, null);
            }
        });

        viewHolder.stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(view. getContext(), "Stop Button Clicked", Toast. LENGTH_SHORT).show();
                tts.stop();
            }
        });

        playAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                StringBuilder builder=new StringBuilder();
                for(int i=0;i<downModels.size();++i){
                    NewsModel item=downModels.get(i);
                    builder.append("Yeni Yazı;");
                    builder.append(new String("Yazar:"+item.getAuthor()+", Başlık:"+item.getTitle()+", Açıklama:"+item.getDescription()+"\n"));
                }
                tts.speak(builder.toString(),TextToSpeech.QUEUE_FLUSH, null);
                Toast.makeText(view. getContext(),"Play All Clicked",Toast.LENGTH_SHORT).show();
                //Toast.makeText(view. getContext(),builder.toString(),Toast.LENGTH_LONG).show();
            }
        });

        pauseAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(view. getContext(),"Pause All Clicked",Toast.LENGTH_SHORT).show();
                tts.stop();
            }
        });

    }

    @Override
    public int getItemCount() {
        return downModels.size();
    }

    public void stopAll(){
        tts.stop();
    }




}
