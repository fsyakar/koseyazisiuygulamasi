package com.example.newsdashboard.holder;

import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.newsdashboard.R;

public class ListenNewsViewHolder extends RecyclerView.ViewHolder {
    public TextView title;
    public TextView author;
    public ImageButton play;
    public ImageButton stop;

    public ListenNewsViewHolder(@NonNull View itemView){
        super(itemView);
        title = itemView.findViewById(R.id.newsTitle);
        author = itemView.findViewById(R.id.newsAuthor);
        play = itemView.findViewById(R.id.playButton);
        stop = itemView.findViewById(R.id.pauseButton);
    }
}