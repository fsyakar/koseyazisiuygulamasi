package com.example.newsdashboard.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.newsdashboard.R;

public class InformationActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_information);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN );

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Bilgiler");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        String text="Uygulamanın kullanım talimatları şu şekildedir:\n\n" +
                "Öncelikle kullanıcı, arayüzde \"Ayarlar>Yazar Ekleme / Çıkarma\" kısmına tıklamalı ve varolan yazarlar arasından takip etmek istediği yazarları seçip eklemelidir.\n\n" +
                "Kullanıcı gazete yazılarının arka planda güncellenmesini istiyorsa \"Ayarlar\" kısmından \"Günlük Güncelleme\" özelliğini aktif edip istediği saati girmelidir.\n\n"+
                "Kullanıcı en güncel haberleri okumak için ana arayüzde bulunan sağ üstteki gazete simgesine dokunarak haberleri güncelleyebilir.\n\n" +
                "Kullanıcı mevcut gazete yazılarını okumak için \"Köşe Yazılarım>Köşe Yazısı\" kısmına dokunmalıdır.\n\n" +
                "Kullanıcı mevcut tüm gazete yazılarını dinlemek için \"Köşe Yazılarım\" kısmına dokunmalı ve ardından üstteki \"Sırayla Oynat\" butonuna dokunmalıdır.\n\n" +
                "Kullanıcı belirli bir gazete yazısını dinlemek için \"Köşe Yazılarım>Köşe Yazısı\" kısmına dokunmalı ve ardından en alttaki oynat ikonuna dokunmalıdır. \n\n" +
                "Kullanıcı varsayılan olarak bir hafta boyunca güncellenen yazılar arasında konu araması yapmak istiyorsa \"Konuya Göre Ara\" kısmındaki arama kısmından arayabilir. Eğer bir haftalık süreci değiştirmek istiyorsa \"Ayarlar\" kısmındaki \"Yazı Tutma Sınırı\"nı 1-20 aralığında değiştirebilir.\n\n" +
                "Not: Eğer dinleme esnasından bir sorun meydana geliyor veya Türkçe dinleme gerçekleşmiyorsa, kullanıcı telefonunun ayarlarındaki yazıdan sese çeviri (Text-to-Speech) kısmından \"Google TTS\" ve Türkçe dilini seçmelidir.\n\n"+
                "Not2: Eğer sayfalarda uzama veya kayma sorunu varsa, kullanıcı telefonunun ayarlarından ekran büyütme özelliğini kapatmalı veya en küçük duruma getirmelidir.";
        textView = (TextView) findViewById(R.id.textview_info);
        textView.setText(text);
        textView.setTextSize(20);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(InformationActivity.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
