package com.example.newsdashboard.model;

public class SettingModel {
    private String updateInterval;

    public SettingModel(){
        updateInterval="";
    }

    public SettingModel(String updateInterval){
        this.updateInterval=updateInterval;
    }

    public String getSetting(){
        return this.updateInterval;
    }

    @Override
    public String toString() {
        return "settingModel{" +
                "updateInterval='" + updateInterval + '\'' +
                '}';
    }
}
