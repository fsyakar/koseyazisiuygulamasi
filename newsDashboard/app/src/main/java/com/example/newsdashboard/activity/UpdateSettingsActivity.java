package com.example.newsdashboard.activity;

import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.NumberPicker;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;
import androidx.appcompat.widget.Toolbar;

import com.example.newsdashboard.R;
import com.example.newsdashboard.database.DatabaseHelper;
import com.example.newsdashboard.model.FilterDayModel;
import com.example.newsdashboard.model.SettingModel;
import com.example.newsdashboard.receiver.AlarmReceiver;

import java.util.Calendar;
import java.util.List;

public class UpdateSettingsActivity extends AppCompatActivity {

    private DatabaseHelper databaseHelperSettings;
    private Toolbar toolbar;
    private AlarmReceiver alarm;
    private NumberPicker np;
    private ImageButton changeButton;

    private SwitchCompat switchCompat;
    public int retDifMinute(int hour1,int minutes1,int hour2,int minutes2){
        int hour;
        int minutes;
        if(hour1<hour2 || (hour1==hour2 && minutes1<minutes2)){
            hour=hour2-hour1;
        }
        else{
            hour = 24-hour1+hour2;
        }
        minutes=minutes2-minutes1;
        return hour*60+minutes;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_settings);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN );

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Güncelleme Ayarı");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        databaseHelperSettings = new DatabaseHelper(UpdateSettingsActivity.this);

        np = findViewById(R.id.num_picker);
        np.setMinValue(1);
        np.setMaxValue(20);
        np.setValue(databaseHelperSettings.getAllFilterDays().get(0).getFilterLimit());

        changeButton = findViewById(R.id.hold_day_button);
        changeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FilterDayModel fdModel=databaseHelperSettings.getAllFilterDays().get(0);
                databaseHelperSettings.deleteAllFilterDays();
                databaseHelperSettings.insertFilterDay(fdModel.getFilterDay(),np.getValue());
                Toast.makeText(UpdateSettingsActivity.this,"Gün Limiti "+np.getValue()+" Olarak Değiştirildi!",Toast.LENGTH_LONG).show();
            }
        });

        switchCompat = findViewById(R.id.update_switch);

        //Toast.makeText(SettingsActivity.this,databaseHelperSettings.getAllSettings().get(0).getSetting(),Toast.LENGTH_LONG).show();
        switchCompat.setChecked(!databaseHelperSettings.getAllSettings().get(0).getSetting().equals("0"));

        switchCompat.setOnCheckedChangeListener(
                new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                        if(switchCompat.isChecked()){
                            List<SettingModel> settingsArray=databaseHelperSettings.getAllSettings();
                            if(!settingsArray.isEmpty()){
                                databaseHelperSettings.deleteSetting(settingsArray.get(0).getSetting());
                            }
                            final Calendar cal = Calendar.getInstance();
                            int hour = cal.get(Calendar.HOUR_OF_DAY);
                            int minute = cal.get(Calendar.MINUTE);


                            TimePickerDialog tpd = new TimePickerDialog(UpdateSettingsActivity.this,
                                new TimePickerDialog.OnTimeSetListener() {
                                    @Override
                                    public void onTimeSet(TimePicker view, int hourOfDay, int minuteOfHour) {
                                        //Toast.makeText(UpdateSettingsActivity.this,hour+" "+minute+" | "+hourOfDay+" "+minuteOfHour,Toast.LENGTH_LONG).show();
                                        int differenceHours = retDifMinute(hour,minute,hourOfDay,minuteOfHour);
                                        if(databaseHelperSettings.insertSetting(Integer.toString(differenceHours))){
                                            Toast.makeText(UpdateSettingsActivity.this,"Günlük Güncelleme Açıldı!",Toast.LENGTH_LONG).show();
                                            //BUNLAR AÇILMALI SERVİS İÇİN todo: alarm=new AlarmReceiver kısmını yukarda yapabilirim
                                            alarm = new AlarmReceiver();
                                            alarm.setAlarm(UpdateSettingsActivity.this);
                                        }
                                        else{
                                            Toast.makeText(UpdateSettingsActivity.this,"Günlük Güncelleme Açılamadı!",Toast.LENGTH_LONG).show();
                                        }

                                    }
                                }, hour, minute, true);

                            tpd.setButton(TimePickerDialog.BUTTON_POSITIVE, "Seç", tpd);
                            tpd.setButton(TimePickerDialog.BUTTON_NEGATIVE, "İptal", tpd);
                            tpd.setOnCancelListener(new DialogInterface.OnCancelListener() {
                                @Override
                                public void onCancel(DialogInterface dialog) {
                                    switchCompat.setChecked(false);
                                }
                            });

                            tpd.setTitle("Güncelleme Saati Seçiniz.");
                            tpd.show();
                        }
                        else{
                            List<SettingModel> settingsArray=databaseHelperSettings.getAllSettings();
                            if(!settingsArray.isEmpty()){
                                databaseHelperSettings.deleteSetting(settingsArray.get(0).getSetting());
                            }
                            if(databaseHelperSettings.insertSetting("0")){
                                Toast.makeText(UpdateSettingsActivity.this,"Günlük Güncelleme Kapatıldı!",Toast.LENGTH_LONG).show();
                            }
                            else{
                                Toast.makeText(UpdateSettingsActivity.this,"Günlük Güncelleme Kapatılamadı!",Toast.LENGTH_LONG).show();
                            }
                        }

                    }
                });

        /*switchCompat.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                databaseHelperSettings = new DatabaseHelper(SettingsActivity.this);

                List<SettingModel> settingsArray=databaseHelperSettings.getAllSettings();
                if(!databaseHelperSettings.getAllSettings().isEmpty()){
                    databaseHelperSettings.deleteSetting(settingsArray.get(0).getSetting());
                }
                if(databaseHelperSettings.insertSetting("1440")){
                    Toast.makeText(SettingsActivity.this,"Ayarlar Uygulandı!",Toast.LENGTH_LONG).show();
                    //BUNLAR AÇILMALI SERVİS İÇİN todo: alarm=new AlarmReceiver kısmını yukarda yapabilirim
                    alarm = new AlarmReceiver();
                    alarm.setAlarm(SettingsActivity.this);
                }
                else{
                    Toast.makeText(SettingsActivity.this,"Ayarlar Uygulanamadı!",Toast.LENGTH_LONG).show();
                }
            }
        });*/
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(UpdateSettingsActivity.this, MainSettingsActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
