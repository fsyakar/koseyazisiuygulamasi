import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:news_dashboard_cross/AuthorsSettings.dart';
import 'package:news_dashboard_cross/ReadNewsDetail.dart';
import 'package:news_dashboard_cross/SqliteHelper.dart';
import 'package:news_dashboard_cross/UpdateSettings.dart';
import 'Models.dart';


class Settings extends StatefulWidget{
  @override
  State<StatefulWidget> createState() => SettingsState();

}

class SettingsState extends State{
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          ListTile(
            leading: Image.asset("images/update_icon.png",height:40,width:40,) ,
            title: Text(
              "Günlük Güncelleme",
              style: TextStyle(
                fontWeight: FontWeight.w400, fontSize: 18.0,
                fontStyle: FontStyle.normal,
                color: Colors.black,
              ),
            ),
            trailing: Icon(Icons.arrow_forward_ios,),
            onTap: (){
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => UpdateSettingsPage()),
              );
            },
          ),
          Divider(height: 15.0,),
          ListTile(
            leading: Image.asset("images/poem.png",height:40,width:40,) ,
            title: Text(
              "Yazar Ekleme / Çıkarma",
              style: TextStyle(
                fontWeight: FontWeight.w400, fontSize: 18.0,
                fontStyle: FontStyle.normal,
                color: Colors.black,
              ),
            ),
            trailing: Icon(Icons.arrow_forward_ios,),
            onTap: (){
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => AuthorsPage()),
              );
            }
          ),
          Divider(height: 15.0,),
        ],
      ),
    );
  }

}

class SettingsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Ayarlar"),
          backgroundColor: Color(0xFF1c2552),
        ),
        body:Settings()
    );
  }
}