import 'package:flutter/cupertino.dart';
import "package:path/path.dart";
import 'package:sqflite/sqflite.dart';
import 'Models.dart';


class DatabaseHelper{
  Database database;

  void openDataBase()async{
    // Avoid errors caused by flutter upgrade.
    // Importing 'package:flutter/widgets.dart' is required.
    WidgetsFlutterBinding.ensureInitialized();
    // Open the database and store the reference.
    database = await openDatabase(
      // Set the path to the database. Note: Using the `join` function from the
      // `path` package is best practice to ensure the path is correctly
      // constructed for each platform.
      join(await getDatabasesPath(), DATABASE_NAME),
      // When the database is first created, create a table to store dogs.
      onCreate: (db, version) {
        // Run the CREATE TABLE statement on the database.
        // For setting table
        db.execute(
          "CREATE TABLE $TABLE_NAME ($SETTING_ID INTEGER PRIMARY KEY AUTOINCREMENT, $UPDATE_INTERVAL TEXT)",
        );
        // For authors table
        db.execute(
          "CREATE TABLE $TABLE_NAME2 ($AUTHOR_ID INTEGER PRIMARY KEY AUTOINCREMENT, $NAME TEXT)",
        );
        // For news table
        db.execute(
          "CREATE TABLE $TABLE_NAME3 ($NEWS_ID_DB INTEGER PRIMARY KEY AUTOINCREMENT, $NEWS_ID INTEGER, $AUTHOR TEXT, $TITLE TEXT, $DESCRIPTION TEXT, $URL TEXT, $URL_TO_IMAGE TEXT, $PUBLISHED_AT TEXT, $CONTENT TEXT)",
        );

        db.execute(
          "CREATE TABLE $TABLE_NAME4 ($NEWS_ID_DB INTEGER PRIMARY KEY AUTOINCREMENT, $NEWS_ID INTEGER, $AUTHOR TEXT, $TITLE TEXT, $DESCRIPTION TEXT, $URL TEXT, $URL_TO_IMAGE TEXT, $PUBLISHED_AT TEXT, $CONTENT TEXT)",
        );

        db.execute(
          "CREATE TABLE $TABLE_NAME5 ($FILTER_DAY_ID INTEGER PRIMARY KEY AUTOINCREMENT, $FILTER_DAY INTEGER, $FILTER_LIMIT INTEGER)",
        );
      },
      // Set the version. This executes the onCreate function and provides a
      // path to perform database upgrades and downgrades.
      version: 1,
    );
  }

  Future<void> insertSetting(Setting setting) async {
    // Get a reference to the database.
    final Database db = await database;

    // Insert the Dog into the correct table. You might also specify the
    // `conflictAlgorithm` to use in case the same dog is inserted twice.
    //
    // In this case, replace any previous data.
    await db.insert(
      TABLE_NAME,
      setting.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<List<Setting>> settings() async {
    // Get a reference to the database.
    final Database db = await database;

    // Query the table for all The Dogs.
    final List<Map<String, dynamic>> maps = await db.query(TABLE_NAME);

    // Convert the List<Map<String, dynamic> into a List<Dog>.
    return List.generate(maps.length, (i) {
      return Setting(
        id: maps[i][SETTING_ID],
        updateInterval: maps[i][UPDATE_INTERVAL],
      );
    });
  }

  Future<void> updateSetting(Setting setting) async {
    // Get a reference to the database.
    final db = await database;

    // Update the given Dog.
    await db.update(
      TABLE_NAME,
      setting.toMap(),
      // Ensure that the Dog has a matching id.
      where: "$SETTING_ID = ?",
      // Pass the Dog's id as a whereArg to prevent SQL injection.
      whereArgs: [setting.id],
    );
  }

  Future<void> deleteSetting(int id) async {
    // Get a reference to the database.
    final db = await database;

    // Remove the Dog from the Database.
    await db.delete(
      TABLE_NAME,
      // Use a `where` clause to delete a specific dog.
      where: "$SETTING_ID = ?",
      // Pass the Dog's id as a whereArg to prevent SQL injection.
      whereArgs: [id],
    );
  }

  Future<void> insertAuthor(Author author) async {
    // Get a reference to the database.
    final Database db = await database;

    // Insert the Dog into the correct table. You might also specify the
    // `conflictAlgorithm` to use in case the same dog is inserted twice.
    //
    // In this case, replace any previous data.
    await db.insert(
      TABLE_NAME2,
      author.toMapAutoIncrement(),//todo: burayı değiştirdim
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<List<Author>> authors() async {
    // Get a reference to the database.
    final Database db = await database;

    // Query the table for all The Dogs.
    final List<Map<String, dynamic>> maps = await db.query(TABLE_NAME2);

    // Convert the List<Map<String, dynamic> into a List<Dog>.
    return List.generate(maps.length, (i) {
      return Author(
        id: maps[i][AUTHOR_ID],
        name: maps[i][NAME],
      );
    });
  }

  Future<void> updateAuthor(Author author) async {
    // Get a reference to the database.
    final db = await database;

    // Update the given Dog.
    await db.update(
      TABLE_NAME2,
      author.toMap(),
      // Ensure that the Dog has a matching id.
      where: "$AUTHOR_ID = ?",
      // Pass the Dog's id as a whereArg to prevent SQL injection.
      whereArgs: [author.id],
    );
  }

  Future<void> deleteAuthor(String authorName) async {
    // Get a reference to the database.
    final db = await database;

    // Remove the Dog from the Database.
    await db.delete(
      TABLE_NAME2,
      // Use a `where` clause to delete a specific dog.
      where: "$NAME = ?",
      // Pass the Dog's id as a whereArg to prevent SQL injection.
      whereArgs: [authorName],
    );
  }

  Future<void> insertNews(News news) async {
    // Get a reference to the database.
    final Database db = await database;

    // Insert the Dog into the correct table. You might also specify the
    // `conflictAlgorithm` to use in case the same dog is inserted twice.
    //
    // In this case, replace any previous data.
    await db.insert(
      TABLE_NAME3,
      news.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<List<News>> news() async {
    // Get a reference to the database.
    final Database db = await database;

    // Query the table for all The Dogs.
    final List<Map<String, dynamic>> maps = await db.query(TABLE_NAME3);

    // Convert the List<Map<String, dynamic> into a List<Dog>.
    return List.generate(maps.length, (i) {
      return News(
        idDb: maps[i][NEWS_ID_DB],
        id: maps[i][NEWS_ID],
        author: maps[i][AUTHOR],
        title: maps[i][TITLE],
        description: maps[i][DESCRIPTION],
        url:maps[i][URL],
        urlToImage: maps[i][URL_TO_IMAGE],
        publishedAt: maps[i][PUBLISHED_AT] ,
        Content: maps[i][CONTENT]
      );
    });
  }

  Future<void> updateNews(News news) async {
    // Get a reference to the database.
    final db = await database;

    // Update the given Dog.
    await db.update(
      TABLE_NAME3,
      news.toMap(),
      // Ensure that the Dog has a matching id.
      where: "$NEWS_ID_DB = ?",
      // Pass the Dog's id as a whereArg to prevent SQL injection.
      whereArgs: [news.idDb],
    );
  }

  Future<void> deleteNews(int id) async {
    // Get a reference to the database.
    final db = await database;

    // Remove the Dog from the Database.
    await db.delete(
      TABLE_NAME3,
      // Use a `where` clause to delete a specific dog.
      where: "$NEWS_ID_DB = ?",
      // Pass the Dog's id as a whereArg to prevent SQL injection.
      whereArgs: [id],
    );
  }

  Future deleteAllNews() async {
    // Get a reference to the database.
    /*var list=await this.news();
    for(int i=0;i<list.length;++i){
      await this.deleteNews(i);
    }*/
    //todo: burayı değıstım bir diğer deleteAllFilterableNews'te böyle oldu
    final db = await database;
    db.execute(
      "DELETE FROM $TABLE_NAME3",
    );
  }

  Future<void> insertFilterableNews(News news) async {
    // Get a reference to the database.
    final Database db = await database;

    // Insert the Dog into the correct table. You might also specify the
    // `conflictAlgorithm` to use in case the same dog is inserted twice.
    //
    // In this case, replace any previous data.
    await db.insert(
      TABLE_NAME4,
      news.toMapAutoIncrement(), //todo: burası değişti dikkat
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<List<News>> filterableNews() async {
    // Get a reference to the database.
    final Database db = await database;

    // Query the table for all The Dogs.
    final List<Map<String, dynamic>> maps = await db.query(TABLE_NAME4);

    // Convert the List<Map<String, dynamic> into a List<Dog>.
    return List.generate(maps.length, (i) {
      return News(
          idDb: maps[i][NEWS_ID_DB],
          id: maps[i][NEWS_ID],
          author: maps[i][AUTHOR],
          title: maps[i][TITLE],
          description: maps[i][DESCRIPTION],
          url:maps[i][URL],
          urlToImage: maps[i][URL_TO_IMAGE],
          publishedAt: maps[i][PUBLISHED_AT] ,
          Content: maps[i][CONTENT]
      );
    });
  }

  Future<void> updateFilterableNews(News news) async {
    // Get a reference to the database.
    final db = await database;

    // Update the given Dog.
    await db.update(
      TABLE_NAME4,
      news.toMap(),
      // Ensure that the Dog has a matching id.
      where: "$NEWS_ID_DB = ?",
      // Pass the Dog's id as a whereArg to prevent SQL injection.
      whereArgs: [news.idDb],
    );
  }

  Future<void> deleteFilterableNews(int id) async {
    // Get a reference to the database.
    final db = await database;

    // Remove the Dog from the Database.
    await db.delete(
      TABLE_NAME4,
      // Use a `where` clause to delete a specific dog.
      where: "$NEWS_ID_DB = ?",
      // Pass the Dog's id as a whereArg to prevent SQL injection.
      whereArgs: [id],
    );
  }

  Future deleteAllFilterableNews() async {
    final db = await database;
    db.execute(
      "DELETE FROM $TABLE_NAME4",
    );
  }

  Future<void> insertFilterDay(FilterDay filterDay) async {
    // Get a reference to the database.
    final Database db = await database;

    await db.insert(
      TABLE_NAME5,
      filterDay.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<List<FilterDay>> filterDays() async {
    // Get a reference to the database.
    final Database db = await database;

    // Query the table for all The Dogs.
    final List<Map<String, dynamic>> maps = await db.query(TABLE_NAME5);

    // Convert the List<Map<String, dynamic> into a List<Dog>.
    return List.generate(maps.length, (i) {
      return FilterDay(
        id: maps[i][FILTER_DAY_ID],
        filterDay: maps[i][FILTER_DAY],
        filterLimit: maps[i][FILTER_LIMIT]
      );
    });
  }

  Future<void> updateFilterDay(FilterDay filterDay) async {
    // Get a reference to the database.
    final db = await database;

    // Update the given Dog.
    await db.update(
      TABLE_NAME5,
      filterDay.toMap(),
      // Ensure that the Dog has a matching id.
      where: "$FILTER_DAY_ID = ?",
      // Pass the Dog's id as a whereArg to prevent SQL injection.
      whereArgs: [filterDay.id],
    );
  }

  Future<void> deleteFilterDay(int id) async {
    // Get a reference to the database.
    final db = await database;

    // Remove the Dog from the Database.
    await db.delete(
      TABLE_NAME5,
      // Use a `where` clause to delete a specific dog.
      where: "$FILTER_DAY_ID = ?",
      // Pass the Dog's id as a whereArg to prevent SQL injection.
      whereArgs: [id],
    );
  }

}


