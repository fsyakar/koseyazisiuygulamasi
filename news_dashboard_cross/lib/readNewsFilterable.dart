import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:news_dashboard_cross/ReadNewsDetail.dart';
import 'package:news_dashboard_cross/SqliteHelper.dart';
import 'Models.dart';


class ReadFilterableNews extends StatefulWidget{
  @override
  State<StatefulWidget> createState() => ReadNewsFilterableState();

}

class ReadNewsFilterableState extends State{

  List<Widget> cardList = new List();
  List<Widget> strictCardList = new List();
  List<String> cardContentList = new List();
  DatabaseHelper helper=new DatabaseHelper();
  bool setStateRan=false;
  TextEditingController textEditingController= TextEditingController();

  Card newsCard(News news){
    return Card(
      child: ListTile(
        leading: CachedNetworkImage(
            imageUrl: news.urlToImage,
            placeholder: (context, url) => Center(child:SizedBox(width:50,height:50,child:CircularProgressIndicator())),
            errorWidget: (context, url, error) => Icon(Icons.error),
            width:100,
            height:75
        ),
        title: Text(news.title),
        subtitle: Text(news.author),
        //trailing: Icon(Icons.more_vert),
        isThreeLine: true,
        onTap: (){
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => ReadNewsDetail(news: news,)),
          );
        },
      ),
    );
  }

  Future<List<News>> getNewsList()async{
    await helper.openDataBase();
    return await helper.filterableNews();
  }

  createCardList()async{
    List<News> newsList =await getNewsList();

    for(int i=0;i<newsList.length;++i){
      cardList.add(newsCard(newsList[i]));
      strictCardList.add(newsCard(newsList[i]));
      cardContentList.add(newsList[i].author+' '+newsList[i].title+' '+newsList[i].description+' '+newsList[i].Content);
    }
    print(cardList.length);

    setState(() { });
  }

  onItemChanged(String value) {
    setState(() {
      List<Card> newCardList = new List<Card>();
      for(int i=0;i<cardContentList.length;++i){
        if(value==''){
          newCardList.add(strictCardList[i]);
        }
        else if(cardContentList[i].toLowerCase().contains(value.toLowerCase())){
          newCardList.add(strictCardList[i]);
        }
      }
      cardList=newCardList;
    });
  }

  @override
  Widget build(BuildContext context) {
    if(setStateRan==false){
      createCardList();
      setStateRan=true;
    }
    List list=<Widget>[
      Padding(
        padding: const EdgeInsets.all(12.0),
        child: TextField(
          controller: textEditingController,
          decoration: InputDecoration(
            prefixIcon: Icon(Icons.search),
            hintText: 'Aramak İçin Yazınız.',
          ),
          onChanged: onItemChanged,
        ),
      )
    ]+ cardList;

    return SingleChildScrollView(
      child: Column(
        children: list
      ),
    );
  }

}


class ReadNewsFilterablePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Konuya Göre Ara"),
          backgroundColor: Color(0xFF1c2552),
        ),
        body:ReadFilterableNews()
    );
  }

}