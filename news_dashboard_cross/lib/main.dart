import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:news_dashboard_cross/AuthorsSettings.dart';
import 'package:news_dashboard_cross/ListenNews.dart';
import 'package:news_dashboard_cross/ReadNews.dart';
import 'package:news_dashboard_cross/ReadNewsFilterable.dart';
import 'package:news_dashboard_cross/UpdateSettings.dart';
import 'Informations.dart';
import 'Settings.dart';
import 'SqliteHelper.dart';
import 'Models.dart';
import 'NewsRequest.dart';

import 'dart:async';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

DatabaseHelper database = new DatabaseHelper();

void main() {
  createDatabase();
  runApp(MaterialApp(
    title: '',
    home: MyApp(),
  ));

}

void createDatabase()async{
  await database.openDataBase();
  List<Setting> settingList=await database.settings();
  List<FilterDay> filterDayList = await database.filterDays();

  if(settingList.isEmpty){
    await database.insertSetting(Setting(
        id: 0,
        updateInterval: "0"
    ));
  }

  if(settingList.isEmpty){
    await database.insertFilterDay(FilterDay(
        id: 0,
        filterDay: 0,
        filterLimit: 7
    ));
  }
  //print(await database.settings());
}

class MyApp extends StatelessWidget {
  double screenWidth;
  double screenHeight;

  Card makeCard(String imagePath,String Title){
    return Card(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            child: FittedBox(
              child: Image.asset(imagePath,width:screenWidth/7,height:screenWidth/7),
              fit: BoxFit.fill,
            ),
          ),
          ListTile(
            title: Center(
                child:Text(
                  Title,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontWeight: FontWeight.w500, fontSize: 18.0
                  ),
                )
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context)  {
    screenWidth=MediaQuery.of(context).size.width;
    screenHeight=MediaQuery.of(context).size.height;

    //var authorCard = makeCard("images/author_gold.png", "Yazarlar");
    var settingsCard = makeCard("images/settings_gold.png", "Ayarlar");
    var informationCard = makeCard("images/information.png","Bilgiler");
    //var listenCard = makeCard("images/listen_gold.png", "Köşe Yazısı\n     Dinle");
    var newsCard = makeCard("images/news_logo_gold.png", "Köşe Yazılarım");
    var newsFilterableCard = makeCard("images/search_logo_gold.png", "Konuya Göre Ara");

    //double sizedBoxWidth=MediaQuery.of(context).size.width/2.2;
    //double sizedBoxHeight=200;

    var column = Column(
      children: [

        Container(
          margin: EdgeInsets.only( left:20,),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children:[
              Align(
                child: Text("Köşe Yazısı \nOkuma Uygulaması",
                  style: TextStyle(
                    fontWeight: FontWeight.w500, fontSize: 25.0,
                    color: Color(0xFFc9bc95),
                  ),
                ),
              ),
              // BURDA BASINCA HABER GUNCELLEMESI KISMI OLUYOR !!
              GestureDetector(
                child: Image.asset("images/newspaper_gold.png",height:75,width:75,),
                onTap: ()async{
                  //TODO: Aşağıdaki true false'a döncek
                  showDialog<String>(
                    context: context,
                    builder: (BuildContext context) => AlertDialog(
                      title: const Text('Yazılar Güncelleniyor..'),
                      content: Container(
                        child:Column(
                            children:[
                              CircularProgressIndicator(
                                semanticsLabel: 'Linear progress indicator',
                              )
                            ]
                        ),
                        height:75,
                        width:75
                      )
                    ),
                  );
                  NewsRequest newsRequest = new NewsRequest(database);
                  await newsRequest.updateNews(false);
                  Navigator.pop(context);
                },
              )

            ]
          )
        ),
        Row(
          children: [
            GestureDetector(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => ReadNewsPage()),
                );
              },
              child: Container(
                margin: EdgeInsets.only( left:10, top:10,),
                child: SizedBox(
                  height: 200,
                  width: screenWidth/2.15,
                  child: newsCard,
                ),
              ),
            ),
            GestureDetector(
              onTap: (){
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => ReadNewsFilterablePage()),
                );
              },
              child: Container(
                margin: EdgeInsets.only( left:10, top:10,),
                child: SizedBox(
                  height: 200,
                  width: screenWidth/2.15,
                  child: newsFilterableCard,
                ),
              ),
            )


          ],
        ),
        Row(
          children: [

            GestureDetector(
              onTap: (){
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => SettingsPage()),
                );
              },
              child: Container(
                margin: EdgeInsets.only( left:10, top:10,),
                child: SizedBox(
                  height: 200,
                  width: screenWidth/2.15,
                  child: settingsCard,
                ),
              ),
            ),
            GestureDetector(
              onTap :(){
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => InformationsPage()),
                );
              },
              child: Container(
                margin: EdgeInsets.only( left:10, top:10,),
                child: SizedBox(
                  height: 200,
                  width: screenWidth/2.15,
                  child: informationCard,
                ),
              ),
            ),
          ],
        ),
        /*Row(
          children: [
            GestureDetector(
              onTap: (){
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => ListenNewsPage()),
                );
              },
              child: Container(
                margin: EdgeInsets.only( left:10, top:10,),
                child: SizedBox(
                  height: 200,
                  width: screenWidth/2.15,
                  child: listenCard,
                ),
              ),
            ),
            GestureDetector(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => AuthorsPage()),
                );
              },
              child: Container(
                margin: EdgeInsets.only( left:10, top:10,),
                child: SizedBox(
                  height: 200,
                  width: screenWidth/2.15,
                  child: authorCard,
                ),
              ),
            ),
          ],
        )*/
      ],
    );


    return MaterialApp(
        title: "",
//      home: new Text("Add Google fonts to Flutter App")
        home: Scaffold(appBar: AppBar(
            title: Text("Ana Menü"),
          backgroundColor: Color(0xFF1c2552),
        ),
          body: Container(
            child: SingleChildScrollView(
              child: column,
              padding: EdgeInsets.only(bottom:300,),
            ),
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage("images/bg_blue_gold.png"),
                fit: BoxFit.cover,
              ),
            ),
          ),
        )
    );
  }
}
