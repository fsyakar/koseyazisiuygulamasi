import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class NewsWebView extends StatefulWidget {
  String url;
  NewsWebView(String url){
    this.url=url;
  }
  @override
  NewsWebViewState createState() => NewsWebViewState(url);
}

class NewsWebViewState extends State<NewsWebView> {
  String url;

  NewsWebViewState(String url){
    this.url=url;
  }

  @override
  void initState() {
    super.initState();
    // Enable hybrid composition.
    if (Platform.isAndroid) WebView.platform = SurfaceAndroidWebView();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Web Sayfası"),
          backgroundColor: Color(0xFF1c2552),
        ),
        body:WebView(
          initialUrl: url,
        )
    );
  }
}