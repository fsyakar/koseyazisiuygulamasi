import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class InformationsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Bilgiler"),
        backgroundColor: Color(0xFF1c2552),
      ),
      body:SingleChildScrollView(
        child:Padding(
            padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
            child: Text("Uygulamanın kullanım talimatları şu şekildedir:\n\n" +
                      "Öncelikle kullanıcı, arayüzde \"Ayarlar>Yazar Ekleme / Çıkarma\" kısmına tıklamalı ve varolan yazarlar arasından takip etmek istediği yazarları seçip eklemelidir.\n\n" +
                      "Kullanıcı gazete yazılarının arka planda güncellenmesini istiyorsa \"Ayarlar\" kısmından \"Günlük Güncelleme\" özelliğini aktif edip istediği saati girmelidir.\n\n"+
                      "Kullanıcı en güncel haberleri okumak için ana arayüzde bulunan sağ üstteki gazete simgesine dokunarak haberleri güncelleyebilir.\n\n" +
                      "Kullanıcı mevcut gazete yazılarını okumak için \"Köşe Yazılarım>Köşe Yazısı\" kısmına dokunmalıdır.\n\n" +
                      "Kullanıcı mevcut tüm gazete yazılarını dinlemek için \"Köşe Yazılarım\" kısmına dokunmalı ve ardından üstteki \"Sırayla Oynat\" butonuna dokunmalıdır.\n\n" +
                      "Kullanıcı belirli bir gazete yazısını dinlemek için \"Köşe Yazılarım>Köşe Yazısı\" kısmına dokunmalı ve ardından en alttaki oynat ikonuna dokunmalıdır. \n\n" +
                      "Kullanıcı varsayılan olarak bir hafta boyunca güncellenen yazılar arasında konu araması yapmak istiyorsa \"Konuya Göre Ara\" kısmındaki arama kısmından arayabilir. Eğer bir haftalık süreci değiştirmek istiyorsa \"Ayarlar\" kısmındaki \"Yazı Tutma Sınırı\"nı 1-20 aralığında değiştirebilir.\n\n" +
                      "Not: Eğer dinleme esnasından bir sorun meydana geliyor veya Türkçe dinleme gerçekleşmiyorsa, kullanıcı telefonunun ayarlarındaki yazıdan sese çeviri (Text-to-Speech) kısmından Türkçe dilini seçmelidir.\n\n"+
                      "Not2: Eğer sayfalarda uzama veya kayma sorunu varsa, kullanıcı telefonunun ayarlarından ekran büyütme özelliğini kapatmalı veya en küçük duruma getirmelidir.",
                      style: TextStyle(
                        fontWeight: FontWeight.w400, fontSize: 20.0,
                        fontStyle: FontStyle.normal,
                        color: Colors.black,
                      ),
                )
          ),
      )
    );
  }
}