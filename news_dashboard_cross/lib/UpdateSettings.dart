import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'NewsRequest.dart';
import 'SqliteHelper.dart';
import 'Models.dart';

import 'dart:async';
import 'package:path_provider/path_provider.dart';
import 'package:flutter_isolate/flutter_isolate.dart';
import 'package:numberpicker/numberpicker.dart';


class SettingSwitch extends StatefulWidget {

  SettingSwitchState createState() => SettingSwitchState();

}

class SettingSwitchState extends State{
  DatabaseHelper helper = new DatabaseHelper();
  bool firstInternalFlag = false;
  bool switchValue=false;
  static FlutterIsolate periodicIsolate;
  int _currentValue=7;

  static void isolatePeriodic(int arg){
    getTemporaryDirectory().then((dir) {
      print("isolatePeriodic temporary directory: $dir FOR $arg MINUTES");
    });
    Future.delayed(Duration(minutes: arg), () async {
      print("Timer Delayed $arg minute");
      DatabaseHelper database = new DatabaseHelper();
      await database.openDataBase();
      NewsRequest newsRequest = new NewsRequest(database);
      await newsRequest.updateNews(true);
      //todo: alttaki iki satırı kontrol et!
      await database.deleteSetting(0);
      await database.insertSetting(Setting(id:0,updateInterval: "1440"));
      Timer.periodic(Duration(minutes: 1440), (timer) async {
        print("Timer Running From IsolatePeriodic 1440 minutes");
        DatabaseHelper database = new DatabaseHelper();
        await database.openDataBase();
        NewsRequest newsRequest = new NewsRequest(database);
        await newsRequest.updateNews(true);
      });
    });

  }

  getUpdateIntervalValueFromDB()async{
    await helper.openDataBase();
    List<Setting>future=await helper.settings();
    List<FilterDay> ftList= await helper.filterDays();
    print(future[0].updateInterval);
    print(ftList[0].filterLimit);

    setState(() {
      if(future[0].updateInterval=="0"){
        switchValue=false;
      }
      else{
        switchValue=true;
      }
      _currentValue=ftList[0].filterLimit;

    });
  }

  changeInterval(int newInterval) async{
    await helper.deleteSetting(0);
    await helper.insertSetting(Setting(id:0,updateInterval: newInterval.toString()));
    if(newInterval==0){
      periodicIsolate.kill();
      print("isolate killed");
    }
    else{
      print("isolate started");
      periodicIsolate=await FlutterIsolate.spawn(isolatePeriodic,newInterval);
    }
  }

  int retDifMinute(int hour1,int minutes1,int hour2,int minutes2){
    int hour;
    int minutes;
    if(hour1<hour2 || (hour1==hour2 && minutes1<minutes2)){
      hour=hour2-hour1;
    }
    else{
      hour = 24-hour1+hour2;
    }
    minutes=minutes2-minutes1;
    return hour*60+minutes;
  }

  Widget switchMethod(){
    return Padding(
      padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
      child: Column(
        children: [
          ListTile(
            leading: Image.asset("images/update_icon.png",height:40,width:40,),
            title:Padding(
              padding: EdgeInsets.fromLTRB(10,0,0,0),
              child: Text(
                "Günlük Güncelleme",
                style: TextStyle(
                  fontWeight: FontWeight.w400, fontSize: 18.0,
                  fontStyle: FontStyle.normal,
                  color: Colors.black,
                ),
              ),
            ),
            trailing: Switch(
                value: switchValue,
                activeColor: Colors.green,
                onChanged: (bool newValue)async{
                  TimeOfDay newTime;
                  TimeOfDay nowTime=TimeOfDay.now();
                  int timeDuration;
                  if(!switchValue){
                    newTime=await showTimePicker(
                      context: context,
                      initialTime: nowTime,
                      initialEntryMode: TimePickerEntryMode.dial,
                      cancelText: "İptal",
                      confirmText: "Tamam",
                      helpText: "Güncelleme Saati Giriniz:",
                    );

                    if(newTime==null){
                      newValue=false;
                    }
                    else{
                      timeDuration=retDifMinute(nowTime.hour, nowTime.minute, newTime.hour, newTime.minute);
                    }
                  }
                  setState(() {
                    switchValue=newValue;
                    if(newValue){
                      changeInterval(timeDuration);
                    }
                    else{
                      changeInterval(0);
                    }
                  });
                }
            ),
          ),
          Divider(height: 15.0,),
          ListTile(
            leading: Image.asset("images/calendar.png",height:40,width:40,),
            title:Padding(
              padding: EdgeInsets.fromLTRB(10,0,0,0),
              child: Text(
                "Yazı Tutma Sınırı(Gün)",
                style: TextStyle(
                  fontWeight: FontWeight.w400, fontSize: 18.0,
                  fontStyle: FontStyle.normal,
                  color: Colors.black,
                ),
              ),
            ),
            trailing: ElevatedButton(
              onPressed: ()async{
                List<FilterDay> lst= await helper.filterDays();
                helper.deleteFilterDay(0);
                helper.insertFilterDay(FilterDay(id:0,filterDay:lst[0].filterDay,filterLimit: _currentValue));
              },
              child: Container(
                  child:Text(
                    'Değiştir',
                    style: TextStyle(fontSize: 12),
                    textAlign: TextAlign.center,
                  ),
              ),
              style: ButtonStyle(
                backgroundColor:MaterialStateProperty.all<Color>(Color(0xFF1c2552)),
              ),
            ),
          ),
          NumberPicker(
            value: _currentValue,
            minValue: 1,
            maxValue: 20,
            axis:Axis.horizontal,
            onChanged: (value) => setState(() => _currentValue = value),
          ),
          Divider(height: 15.0,),
        ],
      )
    );
  }

  @override
  Widget build(BuildContext context) {
    if (!firstInternalFlag) {
      print("buraya girdi");
      getUpdateIntervalValueFromDB();
      firstInternalFlag = true;
    }

    return switchMethod();
  }

}

class UpdateSettingsPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Güncelleme Ayarı"),
          backgroundColor: Color(0xFF1c2552),
        ),
        body: SettingSwitch()
    );
  }
}