import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_tts/flutter_tts.dart';
import 'package:news_dashboard_cross/SqliteHelper.dart';
import 'Models.dart';

import 'dart:io' show Platform;
import 'package:flutter/foundation.dart' show kIsWeb;
import 'dart:async';



class ListenNews extends StatefulWidget{
  @override
  State<StatefulWidget> createState() => ListenNewsState();

}

enum TtsState { playing, stopped, paused, continued }

class ListenNewsState extends State{

  List<Widget> cardList = new List();
  List<News> newsList;
  DatabaseHelper helper=new DatabaseHelper();
  bool setStateRan=false;

  FlutterTts flutterTts;
  String language;
  double volume = 0.5;
  double pitch = 1.0;
  double rate = 0.5;
  bool isCurrentLanguageInstalled = false;

  String _newVoiceText;

  TtsState ttsState = TtsState.stopped;

  get isPlaying => ttsState == TtsState.playing;
  get isStopped => ttsState == TtsState.stopped;
  get isPaused => ttsState == TtsState.paused;
  get isContinued => ttsState == TtsState.continued;

  bool get isIOS => !kIsWeb && Platform.isIOS;
  bool get isAndroid => !kIsWeb && Platform.isAndroid;
  bool get isWeb => kIsWeb;

  @override
  initState() {
    super.initState();
    initTts();
  }

  Future _getEngines() async {
    var engines = await flutterTts.getEngines;
    if (engines != null) {
      for (dynamic engine in engines) {
        print(engine);
      }
    }
  }

  initTts() async{
    flutterTts = FlutterTts();

    if (isAndroid) {
      _getEngines();
    }

    flutterTts.setStartHandler(() {
      setState(() {
        print("Playing");
        ttsState = TtsState.playing;
      });
    });

    flutterTts.setCompletionHandler(() {
      setState(() {
        print("Complete");
        ttsState = TtsState.stopped;
      });
    });

    flutterTts.setCancelHandler(() {
      setState(() {
        print("Cancel");
        ttsState = TtsState.stopped;
      });
    });

    if (isWeb || isIOS) {
      flutterTts.setPauseHandler(() {
        setState(() {
          print("Paused");
          ttsState = TtsState.paused;
        });
      });

      flutterTts.setContinueHandler(() {
        setState(() {
          print("Continued");
          ttsState = TtsState.continued;
        });
      });
    }

    flutterTts.setErrorHandler((msg) {
      setState(() {
        print("error: $msg");
        ttsState = TtsState.stopped;
      });
    });

    await flutterTts.setVolume(volume);
    await flutterTts.setSpeechRate(rate);
    await flutterTts.setPitch(pitch);
    await flutterTts.setLanguage("tr-TR");
  }

  Card firstCard(String allString){
    return Card(
      child: Row(
        children: [
              Padding(
                child: ElevatedButton(
                  onPressed: ()async {
                      var result = await flutterTts.speak(allString);
                      if (result == 1) {
                        setState(() => ttsState = TtsState.playing);
                      }
                  },
                  child: Container(
                    child:Text(
                        'HEPSİNİ OYNAT',
                        style: TextStyle(fontSize: 10)
                    ),
                    width:MediaQuery.of(context).size.width/4.3,
                    height:30
                  ),
                  style: ButtonStyle(
                    backgroundColor:MaterialStateProperty.all<Color>(Color(0xFF1c2552)),
                  ),
                ),
                padding: EdgeInsets.all(1.5),
              ),
              Padding(
                child: ElevatedButton(
                  onPressed: ()async {
                    var result = await flutterTts.pause();
                    if (result == 1) {
                      setState(() => ttsState = TtsState.paused);
                    }
                  },
                  child: Container(
                      child:Text(
                          'HEPSİNİ DURAKLAT',
                          style: TextStyle(fontSize: 10)
                      ),
                      width:MediaQuery.of(context).size.width/4.3,
                      height:30
                  ),
                  style: ButtonStyle(
                    backgroundColor:MaterialStateProperty.all<Color>(Color(0xFF1c2552)),
                  ),
                ),
                padding: EdgeInsets.all(1.5),
              ),
              Padding(
                child: ElevatedButton(
                  onPressed: ()async {
                    var result = await flutterTts.stop();
                    if (result == 1) {
                      setState(() => ttsState = TtsState.stopped);
                    }
                  },
                  child: Container(
                      child:Text(
                          'HEPSİNİ DURDUR',
                          style: TextStyle(fontSize: 10)
                      ),
                      width:MediaQuery.of(context).size.width/4.3,
                      height:30
                  ),
                  style: ButtonStyle(
                    backgroundColor:MaterialStateProperty.all<Color>(Color(0xFF1c2552)),
                  ),
                ),
                padding: EdgeInsets.all(1.5),
              ),
            ],
          ),
        );
  }

  Card newsCard(News news){
    return Card(
      child: ListTile(
        leading: Container(
          width:145,
          height: 50,
          child: Row(
            children: [
              IconButton(
                onPressed: ()async{
                  var result = await flutterTts.speak(news.author+","+news.title+","+news.description);
                  if (result == 1) {
                    setState(() => ttsState = TtsState.playing);
                  }
                },
                icon: Image.asset("images/play.png",height:40,width:40,),
                padding: EdgeInsets.all(1),
              ),
              IconButton(
                onPressed: ()async{
                  var result = await flutterTts.pause();
                  if (result == 1) {
                    setState(() => ttsState = TtsState.paused);
                  }
                },
                icon: Image.asset("images/pause.png",height:40,width:40,),
                padding: EdgeInsets.all(1),
              ),
              IconButton(
                onPressed:  ()async{
                  var result = await flutterTts.stop();
                  if (result == 1) {
                    setState(() => ttsState = TtsState.stopped);
                  }
                },
                icon: Image.asset("images/stop.png",height:40,width:40,),
                padding: EdgeInsets.all(1),
              )
            ],
          ),
        ),
        title: Text(news.title),
        subtitle: Text(news.author),
        trailing: Icon(Icons.music_note),

      ),
    );
  }

  Future<List<News>> getNewsList()async{
    await helper.openDataBase();
    return await helper.news();
  }

  createCardList()async{
    newsList =await getNewsList();

    String allString="";
    for(int i=0;i<newsList.length;++i){
      allString=allString+newsList[i].author+","+newsList[i].title+","+newsList[i].description+"\n";
    }
    cardList.add(firstCard(allString));
    for(int i=0;i<newsList.length;++i){
      cardList.add(newsCard(newsList[i]));
    }
    print(cardList.length);

    setState(() { });
  }

  @override
  Widget build(BuildContext context) {
    if(setStateRan==false){
      createCardList();
      setStateRan=true;
    }


    return SingleChildScrollView(
      child: Column(
        children: cardList,
      ),
    );
  }

}

class ListenNewsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Köşe Yazısı Dinle"),
          backgroundColor: Color(0xFF1c2552),
        ),
        body:ListenNews()
    );
  }

}