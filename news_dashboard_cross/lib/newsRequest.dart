import 'dart:convert' as convert;
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import "SqliteHelper.dart";
import "Models.dart";


class NewsRequest{
  DatabaseHelper database;

  NewsRequest(DatabaseHelper database){
    this.database = database;
  }

  updateNews(bool isService)async{
    await database.deleteAllNews();
    List<FilterDay> listFilter = await database.filterDays();
    int filterDayNum= listFilter[0].filterDay;
    int filterDayLimit=listFilter[0].filterLimit;
    await database.deleteFilterDay(0);
    print("filterdaynum: $filterDayNum ,filterdaylimit: $filterDayLimit");
    if(filterDayNum>=filterDayLimit){
      await database.insertFilterDay(FilterDay(id:0,filterDay: 0,filterLimit: filterDayLimit));
      await database.deleteAllFilterableNews();
    }
    else if(isService){
      await database.insertFilterDay(FilterDay(id:0,filterDay: filterDayNum+1,filterLimit: filterDayLimit));
    }
    else{
      await database.insertFilterDay(FilterDay(id:0,filterDay: filterDayNum,filterLimit: filterDayLimit));
    }

    List<Author> listAuthor=await database.authors();
    for(int i=0;i<listAuthor.length;++i){
      await request(listAuthor[i].name);
    }
  }


  request(String author)async{
    String newsLink = "";
    if(author.contains("Ertuğrul Özkök") || author.contains("Ahmet Hakan")){
      String authorLinkName="";
      if(author.contains("Ertuğrul Özkök")){
        authorLinkName="ertugrul-ozkok";
      }
      else if(author.contains("Ahmet Hakan")){
        authorLinkName="ahmet-hakan";
      }

      final response = await http.Client().get(Uri.parse('https://www.hurriyet.com.tr/yazarlar/$authorLinkName/'));

      if(response.statusCode == 200){
        String rp = response.body;
        for(int i=rp.indexOf(RegExp("yazarlar/$authorLinkName/[a-zA-Z0-9]"));rp[i]!="\"";++i){
          newsLink=newsLink+rp[i];
        }
        newsLink="https://www.hurriyet.com.tr/"+newsLink;
      }
      else{
        print('Inner request failed with status: ${response.statusCode}.');
      }
    }
    else if(author.contains("Taha Akyol") || author.contains("Mehmet Ocaktan") || author.contains("Yusuf Ziya Cömert") || author.contains("Elif Çakır")){
      String authorLinkName="";
      if(author.contains("Taha Akyol")){
        authorLinkName="taha-akyol";
      }
      else if(author.contains("Mehmet Ocaktan")){
        authorLinkName="mehmet-ocaktan";
      }
      else if(author.contains("Yusuf Ziya Cömert")){
        authorLinkName="yusuf-ziya-comert";
      }
      else if(author.contains("Elif Çakır")){
        authorLinkName="elif-cakir";
      }

      final response = await http.Client().get(Uri.parse('https://www.karar.com/yazarlar/$authorLinkName'));

      if(response.statusCode == 200){
        String rp = response.body;
        for(int i=rp.indexOf(RegExp("/yazarlar/$authorLinkName/"));rp[i]!="\"";++i){
          newsLink=newsLink+rp[i];
        }
        newsLink="https://www.karar.com"+newsLink;
      }
      else{
        print('Inner request failed with status: ${response.statusCode}.');
      }
    }
    else if(author.contains("Mehmet Barlas") || author.contains("Salih Tuna") || author.contains("Engin Ardıç") || author.contains("Kerem Alkin") || author.contains("Sait Gürsoy") || author.contains("Hıncal Uluç") || author.contains("Halit Yerebakan") || author.contains("Hilal Kaplan")){
      String authorLinkName="";
      if(author.contains("Mehmet Barlas")){
        authorLinkName="barlas";
      }
      else if(author.contains("Salih Tuna")){
        authorLinkName="salih-tuna";
      }
      else if(author.contains("Engin Ardıç")){
        authorLinkName="ardic";
      }
      else if(author.contains("Kerem Alkin")){
        authorLinkName="kerem-alkin";
      }
      else if(author.contains("Sait Gürsoy")){
        authorLinkName="gursoy";
      }
      else if(author.contains("Hıncal Uluç")){
        authorLinkName="uluc";
      }
      else if(author.contains("Hilal Kaplan")){
        authorLinkName="hilalkaplan";
      }
      else if(author.contains("Halit Yerebakan")){
        authorLinkName="halit-yerebakan";
      }

      final response = await http.Client().get(Uri.parse('https://www.sabah.com.tr/yazarlar/$authorLinkName'));

      if(response.statusCode == 200){
        String rp = response.body;
        for(int i=rp.indexOf(RegExp("yazarlar/$authorLinkName/[0-9]"));rp[i]!="\"";++i){
          newsLink=newsLink+rp[i];
        }
        newsLink="https://www.sabah.com.tr/"+newsLink;
      }
      else{
        print('Inner request failed with status: ${response.statusCode}.');
      }
    }
    else if(author.contains("Yılmaz Özdil") || author.contains("Uğur Dündar") || author.contains("Emin Çölaşan") || author.contains("Rahmi Turan") || author.contains("Bekir Coşkun") || author.contains("Necati Doğru") || author.contains("Saygı Öztürk") || author.contains("Soner Yalçın")){
      String authorLinkName="";
      if(author.contains("Uğur Dündar")){
        authorLinkName="ugur-dundar";
      }
      else if(author.contains("Yılmaz Özdil")){
        authorLinkName="yilmaz-ozdil";
      }
      else if(author.contains("Emin Çölaşan")){
        authorLinkName="emin-colasan";
      }
      else if(author.contains("Rahmi Turan")){
        authorLinkName="rahmi-turan";
      }
      else if(author.contains("Bekir Coşkun")){
        authorLinkName="bekir-coskun";
      }
      else if(author.contains("Necati Doğru")){
        authorLinkName="necati-dogru";
      }
      else if(author.contains("Saygı Öztürk")){
        authorLinkName="saygi-ozturk";
      }
      else if(author.contains("Soner Yalçın")){
        authorLinkName="soner-yalcin";
      }

      final response = await http.Client().get(Uri.parse('https://www.sozcu.com.tr/2021/yazarlar/$authorLinkName'));

      if(response.statusCode == 200) {
        String rp = response.body;
        for (int i = rp.indexOf("https://www.sozcu.com.tr/2021/yazarlar/$authorLinkName/"); rp[i] != "\""; ++i) {
          newsLink = newsLink + rp[i];
        }
      }
      else {
        print('Inner request failed with status: ${response.statusCode}.');
      }
    }
    else if(author.contains("Fatih Altaylı") || author.contains("Murat Bardakçı") || author.contains("Nagehan Alçı") || author.contains("Kübra Par") || author.contains("Sevilay Yılman") || author.contains("Nihal Bengisu Karaca")){
      String authorLinkName="";
      if(author.contains("Fatih Altaylı")){
        authorLinkName="fatih-altayli-1001";
      }
      else if(author.contains("Murat Bardakçı")){
        authorLinkName="murat-bardakci";
      }
      else if(author.contains("Nagehan Alçı")){
        authorLinkName="nagehan-alci";
      }
      else if(author.contains("Kübra Par")){
        authorLinkName="kubra-par-2561";
      }
      else if(author.contains("Sevilay Yılman")){
        authorLinkName="sevilay-yilman-2383";
      }
      else if(author.contains("Nihal Bengisu Karaca")){
        authorLinkName="nihal-bengisu-karaca";
      }

      final response = await http.Client().get(Uri.parse('https://www.haberturk.com/htyazar/$authorLinkName'));

      if(response.statusCode == 200) {
        String rp = response.body;
        for (int i = rp.indexOf("/yazarlar/$authorLinkName/"); rp[i] != "\""; ++i) {
          newsLink = newsLink + rp[i];
        }
        newsLink="https://www.haberturk.com"+newsLink;
      }
      else {
        print('Inner request failed with status: ${response.statusCode}.');
      }
    }
    else if(author.contains("Abdurrahman Dilipak") || author.contains("Ahmet Gülümseyen") || author.contains("Akif Bedir") || author.contains("Ali Karahasanoğlu") || author.contains("Ayhan Demir") || author.contains("Hüseyin Öztürk") || author.contains("Mustafa Çelik") || author.contains("Vehbi Kara")){
      String authorLinkName="";
      if(author.contains("Abdurrahman Dilipak")){
        authorLinkName="abdurrahman-dilipak";
      }
      else if(author.contains("Ahmet Gülümseyen")){
        authorLinkName="ahmet-gulumseyen";
      }
      else if(author.contains("Akif Bedir")){
        authorLinkName="akif-bedir";
      }
      else if(author.contains("Ali Karahasanoğlu")){
        authorLinkName="ali-karahasanoglu";
      }
      else if(author.contains("Ayhan Demir")){
        authorLinkName="ayhan-demir";
      }
      else if(author.contains("Hüseyin Öztürk")){
        authorLinkName="huseyin-ozturk";
      }
      else if(author.contains("Mustafa Çelik")){
        authorLinkName="mustafa-celik";
      }
      else if(author.contains("Vehbi Kara")){
        authorLinkName="vehbi-kara";
      }

      final response = await http.Client().get(Uri.parse('https://www.yeniakit.com.tr/yazarlar/$authorLinkName'));

      if(response.statusCode == 200) {
        String rp = response.body;
        for (int i = rp.indexOf("https://www.yeniakit.com.tr/yazarlar/$authorLinkName/"); rp[i] != "\""; ++i) {
          newsLink = newsLink + rp[i];
        }
      }
      else {
        print('Inner request failed with status: ${response.statusCode}.');
      }
    }
    else if(author.contains("Tamer Korkmaz") || author.contains("Nedret Ersanel") || author.contains("Bülent Orakoğlu") || author.contains("Ergün Yıldırım") || author.contains("Mehmet Acet") || author.contains("Taha Kılınç") || author.contains("Mustafa Kutlu") || author.contains("Yaşar Süngü") || author.contains("Yasin Aktay") || author.contains("Ahmet Ulusoy")) {
      String authorLinkName = "";
      if (author.contains("Tamer Korkmaz")) {
        authorLinkName = "tamer-korkmaz";
      }
      else if (author.contains("Nedret Ersanel")) {
        authorLinkName = "nedret-ersanel";
      }
      else if (author.contains("Bülent Orakoğlu")) {
        authorLinkName = "bulent-orakoglu";
      }
      else if (author.contains("Ergün Yıldırım")) {
        authorLinkName = "ergun-yildirim";
      }
      else if (author.contains("Mehmet Acet")) {
        authorLinkName = "mehmet-acet";
      }
      else if (author.contains("Taha Kılınç")) {
        authorLinkName = "taha-kilinc";
      }
      else if (author.contains("Mustafa Kutlu")) {
        authorLinkName = "mustafa-kutlu";
      }
      else if (author.contains("Yaşar Süngü")) {
        authorLinkName = "yasar-sungu";
      }
      else if (author.contains("Yasin Aktay")) {
        authorLinkName = "yasin-aktay";
      }
      else if (author.contains("Ahmet Ulusoy")) {
        authorLinkName = "ahmet-ulusoy";
      }

      final response = await http.Client().get(Uri.parse('https://www.yenisafak.com/yazarlar/$authorLinkName'));

      if(response.statusCode == 200) {
        String rp = response.body;
        for (int i = rp.indexOf("/yazarlar/$authorLinkName/"); rp[i] != "\""; ++i) {
          newsLink = newsLink + rp[i];
        }
        newsLink="https://www.yenisafak.com"+newsLink;
      }
      else {
        print('Inner request failed with status: ${response.statusCode}.');
      }

    }
    else if(author.contains("Mustafa Balbay") || author.contains("Adnan Dinçer") || author.contains("Evin İlyasoğlu") || author.contains("Mine Söğüt") || author.contains("Öztin Akgüç") || author.contains("Deniz Yıldırım") || author.contains("Barış Doster") || author.contains("Elçin Poyrazlar")) {
      String authorLinkName = "";
      if (author.contains("Mustafa Balbay")) {
        authorLinkName = "mustafa-balbay";
      }
      else if (author.contains("Adnan Dinçer")) {
        authorLinkName = "adnan-dincer";
      }
      else if (author.contains("Evin İlyasoğlu")) {
        authorLinkName = "evin-ilyasoglu";
      }
      else if (author.contains("Mine Söğüt")) {
        authorLinkName = "mine-sogut";
      }
      else if (author.contains("Öztin Akgüç")) {
        authorLinkName = "oztin-akguc";
      }
      else if (author.contains("Deniz Yıldırım")) {
        authorLinkName = "deniz-yildirim";
      }
      else if (author.contains("Barış Doster")) {
        authorLinkName = "baris-doster";
      }
      else if (author.contains("Elçin Poyrazlar")) {
        authorLinkName = "elcin-poyrazlar";
      }

      final response = await http.Client().get(Uri.parse('https://www.cumhuriyet.com.tr/yazarlar/$authorLinkName/'));

      if(response.statusCode == 200) {
        String rp = response.body;
        for (int i = rp.indexOf("/yazarlar/$authorLinkName/"); rp[i] != "\""; ++i) {
          newsLink = newsLink + rp[i];
        }
        newsLink="https://www.cumhuriyet.com.tr"+newsLink;
      }
      else {
        print('Inner request failed with status: ${response.statusCode}.');
      }

    }

    print(newsLink);

    /* ICERIK DONDUREN API KISMI */

    var response=await http.get(Uri.parse("https://extractnews.herokuapp.com/v0/article?url=$newsLink"),);
    /*Uri.parse("https://news-article-data-extract-and-summarization1.p.rapidapi.com/extract/v2/?url=$newsLink&useCache=true"),
        headers: {"x-rapidapi-key" : "56bf1e2b75msh317f45f1c14a700p116055jsnadb37a244e06","x-rapidapi-host" : "news-article-data-extract-and-summarization1.p.rapidapi.com"});*/

    if (response.statusCode == 200) {
      var jsonResponse = convert.jsonDecode(response.body);
      await parseJSON(jsonResponse,newsLink,author);

    } else {
      print('Outer request failed with status: ${response.statusCode}.');
    }

    /*var response=await http.get(
        Uri.parse("https://bing-news-search1.p.rapidapi.com/news/search?q=$author&count=100&setLang=tr&cc=tr&freshness=Day&originalImg=true&textFormat=Raw&safeSearch=Off"),
        headers: {"x-bingapis-sdk": "true", "x-rapidapi-key" : "56bf1e2b75msh317f45f1c14a700p116055jsnadb37a244e06","x-rapidapi-host":"bing-news-search1.p.rapidapi.com"});

    if (response.statusCode == 200) {
      var jsonResponse = convert.jsonDecode(response.body);
      parseJSON(jsonResponse);

    } else {
      print('Request failed with status: ${response.statusCode}.');
    }*/
  }

  parseJSON(var jsonResponse,String searchUrl,String authorName) async{
    var json = jsonResponse['article'];
    var list=await database.news();
    var listFilterable=await database.filterableNews();
    int startIndex=list.length;
    int filterableStartIndex=listFilterable.length;


    String title=json["title"];
    String imageURL;
    try{
      imageURL=json["top_image"];
    }
    catch(exception){
      imageURL="https://i.stack.imgur.com/mwFzF.png";
    }
    String description=json["text"];
    String content=json["text"];
    String url=searchUrl;
    String author=authorName;//json["source_url"];
    String publishedAt=json["published"];
    if(publishedAt==null){
      publishedAt="Tarih Belirtilmemiş";
    }
    //print("\nBURAYA BAKkk $title,$imageURL,$description,$content,$url,$author,$publishedAt");


    print("NORMAL $startIndex");
    await database.insertNews(News(
        idDb:startIndex,
        id:startIndex,
        author: author,
        title: title,
        description: description,
        url:url,
        urlToImage: imageURL,
        publishedAt: publishedAt,
        Content: content
    ));

    print("FILTERABLE $filterableStartIndex");
    News filterableNews =News(
        idDb:filterableStartIndex,
        id:filterableStartIndex,
        author: author,
        title: title,
        description: description,
        url:url,
        urlToImage: imageURL,
        publishedAt: publishedAt,
        Content: content
    );
    if(!filterableNews.contains(listFilterable)){
      await database.insertFilterableNews(filterableNews);
    }



  }

  /*parseJSON(var jsonResponse) async{
    var json = jsonResponse['value'];
    var list=await database.news();
    var listFilterable=await database.filterableNews();
    int startIndex=list.length;
    int filterableStartIndex=listFilterable.length;

    int loopSize;
    if(json.length<5){
      loopSize=json.length;
    }
    else{
      loopSize=5;
    }

    for(int i=0;i<loopSize;++i){
      String title=json[i]["name"];
      String imageURL;
      try{
        imageURL=json[i]["image"]["contentUrl"];
      }
      catch(exception){
        imageURL="https://i.stack.imgur.com/mwFzF.png";
      }
      String description=json[i]["description"];
      String content=json[i]["description"];
      String url=json[i]["url"];
      String author=json[i]["provider"][0]["name"];
      String publishedAt=json[i]["datePublished"];

      //print("\nBURAYA BAKkk $title,$imageURL,$description,$content,$url,$author,$publishedAt");

      await database.insertNews(News(
        idDb:startIndex+i,
        id:startIndex+i,
        author: author,
        title: title,
        description: description,
        url:url,
        urlToImage: imageURL,
        publishedAt: publishedAt,
        Content: content
      ));

      News filterableNews =News(
          idDb:filterableStartIndex+i,
          id:filterableStartIndex+i,
          author: author,
          title: title,
          description: description,
          url:url,
          urlToImage: imageURL,
          publishedAt: publishedAt,
          Content: content
      );
      if(!filterableNews.contains(listFilterable)){
        await database.insertFilterableNews(filterableNews);
      }


    }
  }*/
}
