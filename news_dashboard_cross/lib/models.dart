final String DATABASE_NAME ="general.db"; // Generic
final String TABLE_NAME ="settings_table";// For settings
final String TABLE_NAME2 ="author_table"; // For authors
final String TABLE_NAME3 ="news_table"; // For news
final String TABLE_NAME4 ="news_table_filter"; // For news
final String TABLE_NAME5 ="filter_day_table"; // For news

final String SETTING_ID="id";
final String UPDATE_INTERVAL="updateInterval";

final String FILTER_DAY_ID="id";
final String FILTER_DAY ="filterDay"; // For filter day
final String FILTER_LIMIT ="filterLimit";

final String AUTHOR_ID="id";
final String NAME="authorName";

final String NEWS_ID_DB="id";
final String NEWS_ID="newsId";
final String AUTHOR="author"; // For
final String TITLE="title";
final String DESCRIPTION="description";
final String URL="url";
final String URL_TO_IMAGE="urlToImage";
final String PUBLISHED_AT="publishedAt";
final String CONTENT="content";

class Setting {
  final int id;
  final String updateInterval;

  Setting({this.id, this.updateInterval});

  Map<String, dynamic> toMap() {
    return {
      SETTING_ID: id,
      UPDATE_INTERVAL: updateInterval,
    };
  }

  @override
  String toString() {
    return 'Setting{id: $id, updateInterval: $updateInterval}';
  }

  getId(){
    return id;
  }
}

class FilterDay {
  final int id;
  final int filterDay;
  final int filterLimit;

  FilterDay({this.id, this.filterDay, this.filterLimit});

  Map<String, dynamic> toMap() {
    return {
      SETTING_ID: id,
      FILTER_DAY: filterDay,
      FILTER_LIMIT: filterLimit
    };
  }

  @override
  String toString() {
    return 'FilterDay{id: $id, day: $filterDay, limit: $filterLimit}';
  }

}

class Author {
  final int id;
  final String name;

  Author({this.id, this.name});

  Map<String, dynamic> toMap() {
    return {
      AUTHOR_ID: id,
      NAME: name,
    };
  }

  Map<String, dynamic> toMapAutoIncrement() {
    return {
      NAME: name,
    };
  }

  bool contains(List<Author> list){
    for(var i in list){
      if(i.name==this.name){
        return true;
      }
    }
    return false;
  }


  @override
  String toString() {
    return 'Author{id: $id, name: $name}';
  }
}

class News {
  final int idDb;
  final int id;
  final String author;
  final String title;
  final String description;
  final String url;
  final String urlToImage;
  final String publishedAt;
  final String Content;

  News({this.idDb, this.id,this.author,this.title,this.description,this.url,this.urlToImage,this.publishedAt,this.Content});

  Map<String, dynamic> toMap() {
    return {
      NEWS_ID_DB: idDb,
      NEWS_ID: id,
      AUTHOR: author,
      TITLE: title,
      DESCRIPTION: description,
      URL:url,
      URL_TO_IMAGE:urlToImage,
      PUBLISHED_AT:publishedAt,
      CONTENT:Content
    };
  }

  Map<String, dynamic> toMapAutoIncrement() {
    return {
      NEWS_ID: id,
      AUTHOR: author,
      TITLE: title,
      DESCRIPTION: description,
      URL:url,
      URL_TO_IMAGE:urlToImage,
      PUBLISHED_AT:publishedAt,
      CONTENT:Content
    };
  }

  bool contains(List<News> list){
    for(var i in list){
      if(i.url==this.url){
        return true;
      }
    }
    return false;
  }

  @override
  String toString() {
    return 'News{idDb: $idDb, id: $id, author: $author, title: $title, description: $description, url: $url, urlToImage: $urlToImage, publishedAt: $publishedAt, content: $Content}';
  }
}