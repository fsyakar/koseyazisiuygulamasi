
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:news_dashboard_cross/SqliteHelper.dart';
import 'package:searchable_dropdown/searchable_dropdown.dart';
import 'Models.dart';

final List<DropdownMenuItem> items = [
  DropdownMenuItem(
    child: Text("Emin Çölaşan | Sözcü"),
    value: "Emin Çölaşan | Sözcü",
  ),
  DropdownMenuItem(
    child: Text("Yılmaz Özdil | Sözcü"),
    value: "Yılmaz Özdil | Sözcü",
  ),
  DropdownMenuItem(
    child: Text("Uğur Dündar | Sözcü"),
    value: "Uğur Dündar | Sözcü",
  ),
  DropdownMenuItem(
    child: Text("Rahmi Turan | Sözcü"),
    value: "Rahmi Turan | Sözcü",
  ),
  DropdownMenuItem(
    child: Text("Bekir Coşkun | Sözcü"),
    value: "Bekir Coşkun | Sözcü",
  ),
  DropdownMenuItem(
    child: Text("Necati Doğru | Sözcü"),
    value: "Necati Doğru | Sözcü",
  ),
  DropdownMenuItem(
    child: Text("Saygı Öztürk | Sözcü"),
    value: "Saygı Öztürk | Sözcü",
  ),
  DropdownMenuItem(
    child: Text("Soner Yalçın | Sözcü"),
    value: "Soner Yalçın | Sözcü",
  ),
  DropdownMenuItem(
    child: Text("Taha Akyol | Karar"),
    value: "Taha Akyol | Karar",
  ),
  DropdownMenuItem(
    child: Text("Mehmet Ocaktan | Karar"),
    value: "Mehmet Ocaktan | Karar",
  ),
  DropdownMenuItem(
    child: Text("Yusuf Ziya Cömert | Karar"),
    value: "Yusuf Ziya Cömert | Karar",
  ),
  DropdownMenuItem(
    child: Text("Elif Çakır | Karar"),
    value: "Elif Çakır | Karar",
  ),
  DropdownMenuItem(
    child: Text("Mehmet Barlas | Sabah"),
    value: "Mehmet Barlas | Sabah",
  ),
  DropdownMenuItem(
    child: Text("Salih Tuna | Sabah"),
    value: "Salih Tuna | Sabah",
  ),
  DropdownMenuItem(
    child: Text("Engin Ardıç | Sabah"),
    value: "Engin Ardıç | Sabah",
  ),
  DropdownMenuItem(
    child: Text("Kerem Alkin | Sabah"),
    value: "Kerem Alkin | Sabah",
  ),
  DropdownMenuItem(
    child: Text("Sait Gürsoy | Sabah"),
    value: "Sait Gürsoy | Sabah",
  ),
  DropdownMenuItem(
    child: Text("Hıncal Uluç | Sabah"),
    value: "Hıncal Uluç | Sabah",
  ),
  DropdownMenuItem(
    child: Text("Halit Yerebakan | Sabah"),
    value: "Halit Yerebakan | Sabah",
  ),
  DropdownMenuItem(
    child: Text("Hilal Kaplan | Sabah"),
    value: "Hilal Kaplan | Sabah",
  ),
  DropdownMenuItem(
    child: Text("Mustafa Balbay | Cumhuriyet"),
    value: "Mustafa Balbay | Cumhuriyet",
  ),
  DropdownMenuItem(
    child: Text("Adnan Dinçer | Cumhuriyet"),
    value: "Adnan Dinçer | Cumhuriyet",
  ),
  DropdownMenuItem(
    child: Text("Evin İlyasoğlu | Cumhuriyet"),
    value: "Evin İlyasoğlu | Cumhuriyet",
  ),
  DropdownMenuItem(
    child: Text("Mine Söğüt | Cumhuriyet"),
    value: "Mine Söğüt | Cumhuriyet",
  ),
  DropdownMenuItem(
    child: Text("Öztin Akgüç | Cumhuriyet"),
    value: "Öztin Akgüç | Cumhuriyet",
  ),
  DropdownMenuItem(
    child: Text("Deniz Yıldırım | Cumhuriyet"),
    value: "Deniz Yıldırım | Cumhuriyet",
  ),
  DropdownMenuItem(
    child: Text("Barış Doster | Cumhuriyet"),
    value: "Barış Doster | Cumhuriyet",
  ),
  DropdownMenuItem(
    child: Text("Elçin Poyrazlar | Cumhuriyet"),
    value: "Elçin Poyrazlar | Cumhuriyet",
  ),
  DropdownMenuItem(
    child: Text("Fatih Altaylı | Habertürk"),
    value: "Fatih Altaylı | Habertürk",
  ),
  DropdownMenuItem(
    child: Text("Murat Bardakçı | Habertürk"),
    value: "Murat Bardakçı | Habertürk",
  ),
  DropdownMenuItem(
    child: Text("Nagehan Alçı | Habertürk"),
    value: "Nagehan Alçı | Habertürk",
  ),
  DropdownMenuItem(
    child: Text("Kübra Par | Habertürk"),
    value: "Kübra Par | Habertürk",
  ),
  DropdownMenuItem(
    child: Text("Sevilay Yılman | Habertürk"),
    value: "Sevilay Yılman | Habertürk",
  ),
  DropdownMenuItem(
    child: Text("Nihal Bengisu Karaca | Habertürk"),
    value: "Nihal Bengisu Karaca | Habertürk",
  ),
  DropdownMenuItem(
    child: Text("Abdurrahman Dilipak | Yeni Akit"),
    value: "Abdurrahman Dilipak | Yeni Akit",
  ),
  DropdownMenuItem(
    child: Text("Ahmet Gülümseyen | Yeni Akit"),
    value: "Ahmet Gülümseyen | Yeni Akit",
  ),
  DropdownMenuItem(
    child: Text("Akif Bedir | Yeni Akit"),
    value: "Akif Bedir | Yeni Akit",
  ),
  DropdownMenuItem(
    child: Text("Ali Karahasanoğlu | Yeni Akit"),
    value: "Ali Karahasanoğlu | Yeni Akit",
  ),
  DropdownMenuItem(
    child: Text("Ayhan Demir | Yeni Akit"),
    value: "Ayhan Demir | Yeni Akit",
  ),
  DropdownMenuItem(
    child: Text("Hüseyin Öztürk | Yeni Akit"),
    value: "Hüseyin Öztürk | Yeni Akit",
  ),
  DropdownMenuItem(
    child: Text("Mustafa Çelik | Yeni Akit"),
    value: "Mustafa Çelik | Yeni Akit",
  ),
  DropdownMenuItem(
    child: Text("Vehbi Kara | Yeni Akit"),
    value: "Vehbi Kara | Yeni Akit",
  ),
  DropdownMenuItem(
    child: Text("Tamer Korkmaz | Yeni Şafak"),
    value: "Tamer Korkmaz | Yeni Şafak",
  ),
  DropdownMenuItem(
    child: Text("Bülent Orakoğlu | Yeni Şafak"),
    value: "Bülent Orakoğlu | Yeni Şafak",
  ),
  DropdownMenuItem(
    child: Text("Nedret Ersanel | Yeni Şafak"),
    value: "Nedret Ersanel | Yeni Şafak",
  ),
  DropdownMenuItem(
    child: Text("Ergün Yıldırım | Yeni Şafak"),
    value: "Ergün Yıldırım | Yeni Şafak",
  ),
  DropdownMenuItem(
    child: Text("Mehmet Acet | Yeni Şafak"),
    value: "Mehmet Acet | Yeni Şafak",
  ),
  DropdownMenuItem(
    child: Text("Taha Kılınç | Yeni Şafak"),
    value: "Taha Kılınç | Yeni Şafak",
  ),
  DropdownMenuItem(
    child: Text("Mustafa Kutlu | Yeni Şafak"),
    value: "Mustafa Kutlu | Yeni Şafak",
  ),
  DropdownMenuItem(
    child: Text("Yaşar Süngü | Yeni Şafak"),
    value: "Yaşar Süngü | Yeni Şafak",
  ),
  DropdownMenuItem(
    child: Text("Yasin Aktay | Yeni Şafak"),
    value: "Yasin Aktay | Yeni Şafak",
  ),
  DropdownMenuItem(
    child: Text("Ahmet Ulusoy | Yeni Şafak"),
    value: "Ahmet Ulusoy | Yeni Şafak",
  ),
  DropdownMenuItem(
    child: Text("Ertuğrul Özkök | Hürriyet"),
    value: "Ertuğrul Özkök | Hürriyet",
  ),
  DropdownMenuItem(
    child: Text("Ahmet Hakan | Hürriyet"),
    value: "Ahmet Hakan | Hürriyet",
  )
];


class AuthorsPage extends StatefulWidget {
  @override
  AuthorsState createState() => AuthorsState();
}

class AuthorsState extends State<AuthorsPage>  {
  bool databaseFlag = false;
  bool updateDatabaseFlag = false;
  static String selectedValue;
  DatabaseHelper helper = new DatabaseHelper();

  List<SizedBox> sizedBoxList= new List();
  List<String> authorNameList = new List();

  
  Future<List<Author>>getAuthors()async{
    return await helper.authors();
  }

  void updateAuthorsFromDB()async{
    if(!databaseFlag){
      await helper.openDataBase();
      databaseFlag=true;
    }
    //await helper.openDataBase();
    List<Author>dbAuthors=await getAuthors();
    for(int i=0;i<dbAuthors.length;++i){
      sizedBoxList.add(makeBox(dbAuthors[i].name));
      authorNameList.add(dbAuthors[i].name);
    }
    setState(() {});
  }
  
  SizedBox makeBox(String authorName){
    return SizedBox(
      child:Card(
        child:ListTile(
          leading: IconButton(
            icon:Image.asset("images/delete_button.png",height:50,width:50,),
            onPressed: ()async{
              int willRemoveIndex = authorNameList.indexOf(authorName);
              setState(() {
                sizedBoxList.removeAt(willRemoveIndex);
                authorNameList.removeAt(willRemoveIndex);
              });
              await helper.deleteAuthor(authorName);
              print(await helper.authors());
            },
            padding: EdgeInsets.all(1),
          ),
          title: Center(
              child: Text(
                authorName,
                style: TextStyle(
                    fontWeight: FontWeight.w500, fontSize: 20.0),
              )
          ),
        ),
      ),
      width: MediaQuery.of(context).size.width-30,
      height: 80,

    );
  }

  /*void deleteAllAuthors()async{
    List<Author>dbAuthors=await getAuthors();
    for(int i=0;i<dbAuthors.length;++i){
      await helper.deleteAuthor(dbAuthors[i].name);
    }
    print(await helper.authors());
  }*/

  @override
  Widget build(BuildContext context) {
    //deleteAllAuthors();

    if(!updateDatabaseFlag){
      updateAuthorsFromDB();
      updateDatabaseFlag=true;
    }

    Widget widget = Padding(
        padding: EdgeInsets.all(15.0),
        child:SearchableDropdown.single(
          items: items,
          value: selectedValue,
          hint: "Yazar Eklemek İçin Tıklayıp Seçiniz.",
          searchHint: "Aramak İçin Yazın.",
          closeButton: "Kapat",
          onChanged: (value) async {
            print("VALUEEE $value");
            if(value!=null){
              selectedValue = value;
              //print("bak buraya"+selectedValue);
              List<Author> list=await helper.authors();
              var authorObj=Author(
                  id:list.length,
                  name:selectedValue
              );
              if(!authorObj.contains(list)){
                await helper.insertAuthor(authorObj);

                setState(() {
                  if(value!=null){
                    sizedBoxList.add(makeBox(selectedValue));
                    authorNameList.add(selectedValue);
                  }
                  //print(authorNameList);
                });

              }
              print(await helper.authors());
            }
          },
          isExpanded: true,
       )
    );
    

    var authorColumn = Container(
      margin: EdgeInsets.only( left:15,),
        child: Row(
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: sizedBoxList,
            ),
          ],
        ),
    );

    return Scaffold(
      appBar: AppBar(
        title: Text("Yazarlar"),
        backgroundColor: Color(0xFF1c2552),
      ),
      body:SingleChildScrollView(
        child: Column(
        children: [
          widget,
          authorColumn
        ],
      )
      )
    );
  }
}